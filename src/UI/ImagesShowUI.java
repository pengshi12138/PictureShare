package UI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventListener;

import ComponentTool.jButton;
import ImageTool.*;

public class ImagesShowUI extends JFrame {
    //主函数主要组件
    ArrayList<File> ImagesFile;
    String FilePath;
    int choose;//计数器，记录到第几张图片
    int PictureNum;


    //计算图片适合高度宽度
    int PictureH;//可用的窗口大小
    int PictureW;
    int HImage;//改变后的图片大小
    int WImage;
    //暂存图片
    public ImageIcon imageI;
    public Image ImageI;
    //头部工具栏按钮
    jButton AutoPlay;//自动播放
    jButton Big;//放大
    jButton Small;//缩小
    jButton Delete;//删除
    jButton Collection;//收藏
    jButton Rotate;//旋转
    jButton Tailoring;//截屏
    jButton Edit;//编辑
    JButton next;//下一张
    JButton back;//上一张
    jButton fullScreen;//全屏

    //中间图标
    JLabel Picture;
    JPanel PPanel;

    int RotateNum = 90;//旋转角度
    double ScalingIndex = 1;
    //AutoPlay play; //自动播放的函数
    public ImagesShowUI(ArrayList<File> ImagesFile,int choose,String filePath){

        this.ImagesFile = ImagesFile;
        this.choose = choose;
        this.FilePath = ImagesFile.get(choose).getAbsolutePath();
        this.PictureNum = ImagesFile.size();

        //窗口设置
        setTitle("图片查看器");
        Image image = null;
        try {
            image = ImageIO.read(new File("src/Icon/灰图库.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setIconImage(image);

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLayout(new BorderLayout());
        setMinimumSize(new Dimension(1000,650));
        getContentPane().setBackground(Color.WHITE);
        setDefaultLookAndFeelDecorated (true);

        //工具栏设置
        JToolBar bar = new JToolBar("工具栏");
        bar.setBackground(new Color(255,255,255));
        bar.setBorder(null);
        bar.setBorderPainted(false);

        UIManager.put("ToolTip.foreground", new ColorUIResource(Color.black));//设置提示框背景颜色
        UIManager.put("ToolTip.background", new ColorUIResource(Color.white));

        getContentPane().add("North",bar);
        bar.setLayout(new BorderLayout());
        AutoPlay = new jButton("幻灯片放映");
        AutoPlay.setToolTipText("幻灯片放映");
        Big = new jButton("");
        Big.setToolTipText("放大");
        Small = new jButton("");
        Small.setToolTipText("缩小");
        Delete = new jButton("");
        Delete.setToolTipText("删除");
        Collection = new jButton("");
        Collection.setToolTipText("收藏");
        Rotate = new jButton("");
        Rotate.setToolTipText("旋转");
        Tailoring = new jButton("");
        Tailoring.setToolTipText("裁剪");
        Edit = new jButton("编辑&创造");
        Edit.setToolTipText("编辑&创造");
        next = new JButton();
        next.setToolTipText("下一张");
        back = new JButton();
        back.setToolTipText("上一张");

        //设置按钮大小
        AutoPlay.setPreferredSize(new Dimension(130,50));
        Big.setPreferredSize(new Dimension(50,50));
        Small.setPreferredSize(new Dimension(50,50));
        Delete.setPreferredSize(new Dimension(50,50));
        Collection.setPreferredSize(new Dimension(50,50));
        Rotate.setPreferredSize(new Dimension(50,50));
        Tailoring.setPreferredSize(new Dimension(50,50));
        Edit.setPreferredSize(new Dimension(130,50));

        //设置中间五个按钮的面板
        JPanel center = new JPanel(new FlowLayout(1,0,0));
        center.setPreferredSize(new Dimension(300,50));
        center.setBackground(Color.WHITE);
        center.setBorder(null);
        center.add(Big);
        center.add(Small);
        center.add(Delete);
        //center.add(Collection);
        center.add(Rotate);
        center.add(Tailoring);

        //设置右下角全屏按钮的实现
        Big.setIcon(new ImageIcon("src/Icon/黑放大.png"));
        Small.setIcon(new ImageIcon("src/Icon/黑缩小.png"));
        Delete.setIcon(new ImageIcon("src/Icon/黑删除.png"));
        Collection.setIcon(new ImageIcon("src/Icon/黑收藏.png"));
        Rotate.setIcon(new ImageIcon("src/Icon/黑旋转.png"));
        Tailoring.setIcon(new ImageIcon("src/Icon/黑剪切.png"));
        Edit.setIcon(new ImageIcon("src/Icon/黑编辑.png"));
        JPanel FullScreen = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        fullScreen = new jButton("");
        fullScreen.setIcon(new ImageIcon("src/Icon/黑全屏.png"));
        fullScreen.setToolTipText("全屏显示");
        fullScreen.setPreferredSize(new Dimension(50,50));
        FullScreen.add(fullScreen);
        FullScreen.setBackground(Color.WHITE);
        getContentPane().add(BorderLayout.PAGE_END,FullScreen);

        //按钮布局
        bar.add("West",AutoPlay);
        bar.add("Center",center);
        bar.add("East",Edit);

        //设置上下张图片移动
        if (choose == PictureNum - 1){
            //判断图片位置决定按钮显示
            next.setVisible(false);
        }
        if (choose == 0) {
            back.setVisible(false);
        }
        back.setBorder(null);
        back.setBackground(Color.white);
        back.setFocusPainted(false);
        back.setMargin(null);
        ImageIcon l1 = new ImageIcon("src/Icon/灰左箭头.png");
        back.setIcon(l1);
        ImageIcon l2 = new ImageIcon("src/Icon/黑左箭头.png");
        back.setPressedIcon(l2);
        ImageIcon r1 = new ImageIcon("src/Icon/灰右箭头.png");
        next.setIcon(r1);
        ImageIcon r2 = new ImageIcon("src/Icon/黑右箭头.png");
        next.setPressedIcon(r2);
        next.setBorder(null);
        next.setBackground(Color.white);
        next.setFocusPainted(false);
        next.setMargin(null);
        UIManager.put("Button.select",Color.white);//改变按钮按下的颜色
        getContentPane().add("West",back);
        getContentPane().add("East",next);
        pack();
        InitListener();
        setVisible(true);

        //设置幻灯片图标
        ImageIcon a1 = new ImageIcon("src/Icon/黑播放.png");
        AutoPlay.setIcon(a1);

        //右下角全屏模式


        //记录窗口边界
        PictureH = getHeight() - bar.getHeight() - FullScreen.getHeight();
        PictureW = getWidth() - next.getWidth()*2;
//        System.out.println(PictureH+" "+PictureW);
//        System.out.println(getHeight() +" 屏幕 " + getWidth());
//        System.out.println(next.getHeight() + " 下一张 " + next.getWidth());
//        System.out.println(bar.getHeight()+ " 上边框 ");
//        System.out.println(FullScreen.getHeight() + "下边框");

        //初期建立代码
//        FilePath = "D:/2.jpg";
//        File img = new File("D:/2.jpg");
//        imageI = new ImageIcon("D:/2.jpg");

        imageI = new ImageIcon(ImagesFile.get(choose).getAbsolutePath());
        try {
            ImageI = ImageIO.read(ImagesFile.get(choose));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //中间图片显示窗口建立
        PPanel = new JPanel(new FlowLayout());
        PPanel.setBackground(Color.WHITE);
        Picture = new JLabel();
        Picture.setBorder(null);
        PPanel.add("Center",Picture);
        getContentPane().add("Center",PPanel);
        ///Picture.setIcon(imageI);

        //鼠标跟随监听事件
        MyMouseInputAdapter listener=new MyMouseInputAdapter();  //鼠标事件处理
        Picture.addMouseListener(listener);
        Picture.addMouseMotionListener(listener);
        showPicture();
    }
    void InitListener(){
        //放大操作
        Big.addActionListener(e -> {
            Bigger();//放大
        });
        //缩小操作
        Small.addActionListener(e -> {
            Smaller();//缩小
        });
        //旋转操作实现
        Rotate.addActionListener(e -> {
            ImageI = RotateImage.Rotate(ImageI,RotateNum);
            showPicture();
        });
        //删除操作
        Delete.addActionListener(e -> {
            ScalingIndex = 1;
            ImagesFile.get(choose).delete();
            ImagesFile.remove(choose);
            PictureNum--;
            //当选择大于图片总数时候
            if (choose >= PictureNum){
                choose --;
            }
            //当图片等于0时候
            if (PictureNum == 0){
                ImageI =null;
                FilePath =null;
            }else {
                try {
                    ImageI = ImageIO.read(ImagesFile.get(choose));
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                FilePath = ImagesFile.get(choose).getAbsolutePath();
            }
            showPicture();
        });

        //设置两个事件的按钮事件，防止事件冲突互斥导致报错
        ActionListener event = e -> {
            ScalingIndex = 1;
            if (e.getSource() == back && choose > 0){
                try {
                    ImageI = ImageIO.read(ImagesFile.get(--choose));
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                FilePath = ImagesFile.get(choose).getAbsolutePath();
                showPicture();
            }
            else if (e.getSource() == next && choose >= 0 && choose + 1 < PictureNum){
                try {
                    ImageI = ImageIO.read(ImagesFile.get(++choose));
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                FilePath = ImagesFile.get(choose).getAbsolutePath();
                showPicture();
            }
            if (choose == PictureNum - 1){
                next.setVisible(false);
            }else if (choose == 0){
                back.setVisible(false);
            }else{
                next.setVisible(true);
                back.setVisible(true);
            }
        };
        next.addActionListener(event);
        back.addActionListener(event);

        //全屏操作
        fullScreen.addActionListener(e -> new FullScreen(ImageI));

        //幻灯片播放模式
        AutoPlay.addActionListener(e -> new AutoPlayWindow(ImageI,ImagesFile,choose));

        //截屏操作
        Tailoring.addActionListener(e -> new ScreenCapture());

        //编辑操作
        Edit.addActionListener(e -> {
            new CreateUI(ImageI,FilePath).addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    setVisible(true);
                }
            });
            this.setVisible(false);
        });
    }




    public void Bigger(){
        if (ScalingIndex <= 4 && ScalingIndex >= 0.4){
            WImage = (int) (((BufferedImage)ImageI).getWidth() * (ScalingIndex + 0.1));
            HImage = (int) (((BufferedImage)ImageI).getHeight() * (ScalingIndex + 0.1));
            imageI = new ImageIcon(ImageI.getScaledInstance(WImage, HImage, Image.SCALE_DEFAULT));
            Picture.setIcon(imageI);
            ScalingIndex = ScalingIndex + 0.1;
        }
    }
    public void Smaller(){
        if (ScalingIndex <= 4.1 && ScalingIndex >= 0.5){
            WImage = (int) (((BufferedImage)ImageI).getWidth() * (ScalingIndex - 0.1));
            HImage = (int) (((BufferedImage)ImageI).getHeight()  * (ScalingIndex - 0.1));
            imageI = new ImageIcon(ImageI.getScaledInstance(WImage, HImage, Image.SCALE_DEFAULT));
            Picture.setIcon(imageI);
            ScalingIndex = ScalingIndex - 0.1;
        }
    }
    public void showPicture(){
        imageI = new ImageIcon(ImageI);
        double w1,h1;
        HImage = imageI.getIconHeight();
        WImage = imageI.getIconWidth();
        //System.out.println((double) HImage/WImage + " " + (double) PictureH/PictureW);
        //System.out.println(HImage+" "+WImage);
//        if (HImage <= PictureH && WImage <= PictureW){
//            w1 = WImage;
//            h1 = HImage;
//        }
//        else
        if ((double) HImage/WImage > (double) PictureH/PictureW){
            w1 = (double) WImage / HImage * PictureH;
            h1 = PictureH;
        }
        else{
            w1 = PictureW;
            h1 = (double) HImage / WImage * PictureW;
        }
        HImage = (int) h1;
        WImage = (int) w1;

//        System.out.println(w1+" "+h1);
        imageI = new ImageIcon(imageI.getImage().getScaledInstance(WImage, HImage, Image.SCALE_DEFAULT));
        Picture.setIcon(imageI);
//        System.out.println(Picture.getX()+" " +Picture.getY());
//        System.out.println(Picture.getHeight()+" "+Picture.getWidth());

    }



    class MyMouseInputAdapter extends MouseInputAdapter {
        Point point = new Point(0,0); //坐标点

        public void mousePressed(MouseEvent e) {
            point = SwingUtilities.convertPoint(Picture,e.getPoint(),Picture.getParent()); //得到当前坐标点
        }

        public void mouseDragged(MouseEvent e) {
            PictureH = getHeight() - 110;
            PictureW = getWidth() - next.getWidth()*2;
            if (WImage <= PictureW && HImage <= PictureH){
                System.out.println();
            }else {
                Point newPoint = SwingUtilities.convertPoint(Picture,e.getPoint(),Picture.getParent()); //转换坐标系统
                Picture.setLocation(Picture.getX()+(newPoint.x-point.x),Picture.getY()+(newPoint.y-point.y)); //设置标签图片的新位置
                //System.out.println(Picture.getX()+" " + Picture.getY());
                point = newPoint; //更改坐标点
            }
        }
    }
}


