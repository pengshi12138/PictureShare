package UI;

import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;

public class WelcomeWindow extends JWindow {
    public WelcomeWindow() {
        JLabel label_pic = new JLabel(new ImageIcon("src/Data/欢迎界面.jpg"));
        getContentPane().add(label_pic, BorderLayout.CENTER);
        setVisible(true);
        setSize(900, 700);
        setLocationRelativeTo(null);
    }

    public static void start() {
        WelcomeWindow window1 = new WelcomeWindow();
        try {
            Thread.sleep(3000);
            window1.dispose();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

