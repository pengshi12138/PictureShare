package UI;

/**
 * 图片编辑类
 * 特点，使用多线程处理面板
 */

import ComponentTool.DemoScrollBarUI;
import ComponentTool.DemoSliderBarUI;
import ComponentTool.jButton;
import ImageTool.ImageFilters;
import ImageTool.OpencvI;
import com.jhlabs.image.FlipFilter;
import com.jhlabs.image.MirrorFilter;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;

import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class CreateUI extends JFrame {
    ArrayList<JLabel> FilterLabel = new ArrayList<>();
    ArrayList<File> FilterFile = new ArrayList<>();
    String filepath;
    Image SImage;//正在展示的图片
    Image image;//原始图片
    Image FImage;//使用过滤镜的图片
    Image AImage;//调整使用的图片
    Font ti = new Font("微软雅黑",Font.BOLD,16);
    Font ti1 = new Font("微软雅黑",Font.BOLD,13);
    Font ti2 = new Font("微软雅黑",Font.BOLD,10);
    Color transparent = new Color(0,0,0,0);
    //面板设置,共有两个面板
    JPanel LeftPanel = new JPanel(new BorderLayout());
    JPanel RightPanel = new JPanel(new BorderLayout());
    public int FH,FFH = 0;//左边面板高度
    public int FW,FFW = 0;//左边面板宽度
    //左边面板显示图片
    JPanel TopPanel = new JPanel(new BorderLayout());
    JLabel ImageLabel = new JLabel();

    //填充面板
    JPanel TPanel = new JPanel();


    //主题颜色确定
    Color c1 = new Color(43,43,43);
    Color c2 = new Color(34,34,34);



    //设置右边面板实现，进行卡片布局
    CardLayout card = new CardLayout(20,0);//定义卡片布局对象
    String cardName[]={"0","1","2"};//定义字符数组，为卡片命名
    JPanel CardPanel = new JPanel();//设置面板容器
    JPanel cutPanel = new JPanel();//剪切面板
    JPanel FilterPanel = new JPanel(new BorderLayout());//滤镜面板
    JPanel adjustmentPanel = new JPanel(new BorderLayout());//调整面板
    JPanel EndPanel = new JPanel();//右下角面板，保存和取消按钮
    jButton KeepB = new jButton();//保存按钮
    jButton cancel = new jButton();//取消按钮
    JLabel Name = new JLabel();
    JPanel RTopPanel;
    //上面按钮实现
    jButton back = new jButton(new ImageIcon("src/Icon/返回.png"));//返回
    jButton Filter = new jButton(new ImageIcon("src/Icon/FilterTool/阴阳.png"));//滤镜
    jButton cut = new jButton(new ImageIcon("src/Icon/FilterTool/剪刀.png"));//裁剪和旋转
    jButton adjustment = new jButton(new ImageIcon("src/Icon/FilterTool/魔法棒.png"));//调整
    /** 初始化该 CreateUI JFrame */
    public CreateUI(Image image,String filepath) {
        this.SImage = image;
        this.image = image;
        this.filepath = filepath;
        setTitle("图片编辑器");
        Image i = null;
        try {
            i = ImageIO.read(new File("src/Icon/灰图库.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setIconImage(i);
        getContentPane().setLayout(new BorderLayout());
        setSize(new Dimension(1400, 930));
        setMinimumSize(new Dimension(700, 500));
        //getContentPane().setBackground(new Color(43,43,43));
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //设置右边面板
        RightPanel.setPreferredSize(new Dimension(300, 930));
        RightPanel.setMinimumSize(new Dimension(300, 0));
        RightPanel.setMaximumSize(new Dimension(300, Integer.MAX_VALUE));
        RightPanel.setBackground(c1);
        getContentPane().add("East", RightPanel);

        //设置左边面板
        LeftPanel.setPreferredSize(new Dimension(1100, 930));
        LeftPanel.setMinimumSize(new Dimension(200, 0));
        LeftPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        LeftPanel.setBackground(c2);
        getContentPane().add("Center", LeftPanel);
        LeftPanel.add( "Center" ,ImageLabel);

        //填充左边面板区域
        TPanel.setBackground(c2);
        TPanel.setPreferredSize(new Dimension(40,100));
        TPanel.setMaximumSize(new Dimension(100,100));
        LeftPanel.add("West",TPanel);

        //设置面板监听事件，实现动态变化
        this.addComponentListener(new ComponentAdapter() {
            //同步锁一定要加，不然监听事件会重复多次，以至于多次运行，造成卡顿，资源浪费
            @Override
            public  synchronized void componentResized(ComponentEvent e) {
                //super.componentResized(e);
                FH = getHeight()  - 80;
                FW = getWidth() - 16 - 300 - 80;

                if (FH != FFH || FFW != FW){
//                    System.out.println(FH  + " " + FW + " " + FFH + " " + FFW);
                    showPicture(SImage);
                }
                FFH = FH;
                FFW =FW;
            }
        });

        //上面工具栏添加
        LeftPanel.add("North",TopPanel);
        back.setPreferredSize(new Dimension(50,50));
        Filter.setPreferredSize(new Dimension(90,50));
        cut.setPreferredSize(new Dimension(130,50));
        adjustment.setPreferredSize(new Dimension(90,50));
        Filter.setText("滤镜");
        Filter.setFont(ti1);
        Filter.setForeground(Color.WHITE);
        cut.setText("裁剪和旋转");
        cut.setFont(ti1);
        cut.setForeground(Color.WHITE);
        adjustment.setText("调整");
        adjustment.setFont(ti1);
        adjustment.setForeground(Color.WHITE);
        JPanel ToolPanel = new JPanel(new FlowLayout(FlowLayout.CENTER,0,0));
        ToolPanel.setBackground(c2);     //先加个背景颜色

        TopPanel.add("West",back);

        ToolPanel.add(cut);
        ToolPanel.add(Filter);
        ToolPanel.add(adjustment);
        TopPanel.add("Center",ToolPanel);
        back.addActionListener(e -> dispose());
        //右下角两个关键按钮
        KeepB.setBackground(new Color(63,63,63));
        cancel.setBackground(new Color(85,85,85));
        KeepB.setForeground(Color.WHITE);
        cancel.setForeground(Color.WHITE);
        KeepB.setText("保  存");
        cancel.setText("取  消");
        intiRightPanel();
        setLocationRelativeTo(null);//窗体居中显示
        pack();
        setVisible(true);
        //初始化监听器函数
        InitListener();
    }
    /** 初始化右边面板 */
    void intiRightPanel(){
        //卡片布局设置
        CardPanel.setBackground(transparent);//设置透明边框
        CardPanel.setPreferredSize(new Dimension(300,200));
        CardPanel.setMaximumSize(new Dimension(300,Integer.MAX_VALUE));
        CardPanel.setMaximumSize(new Dimension(300,0));
        CardPanel.setLayout(card);
        RightPanel.add(CardPanel);

        CardPanel.add(cutPanel,cardName[0]);
        CardPanel.add(FilterPanel,cardName[1]);
        CardPanel.add(adjustmentPanel,cardName[2]);

        card.show(CardPanel, "1");

        //设置上面显示面板
        RTopPanel = new JPanel();
        RTopPanel.setBackground(c1);
        RTopPanel.setPreferredSize(new Dimension(300,100));
        JPanel line = new JPanel();//设置分割线条
        line.setPreferredSize(new Dimension(250,1));
        line.setBackground(new Color(85,85,85));//new Color(85,85,85)
        Name.setText("滤镜");
        Name.setFont(ti);
        Name.setBackground(transparent);
        Name.setForeground(Color.WHITE);
        Name.setPreferredSize(new Dimension(60,50));
        Name.setVerticalAlignment(JLabel.CENTER);
        Name.setHorizontalAlignment(JLabel.CENTER);

        RTopPanel.add(Name);
        RTopPanel.add(line);
        RightPanel.add("North",RTopPanel);
        //右下角设置
        EndPanel.setPreferredSize(new Dimension(300,50));
        RightPanel.add("South",EndPanel);
        KeepB.setPreferredSize(new Dimension(125,30));
        cancel.setPreferredSize(new Dimension(125,30));
        EndPanel.add(KeepB);
        EndPanel.add(cancel);
        EndPanel.setBackground(transparent);
        //多线程处理面板
        Thread f1 = new IntiFilterPanel();
        Thread f2 = new IntiCutPanel();
        Thread f3 = new IntiAdjustmentPanel();
        f1.start();
        f2.start();
        f3.start();
    }
    /** 多线程处理面板  初始化滤镜面板*/
    class IntiFilterPanel extends Thread{
        public void run(){
            //滤镜面板布局
            FilterPanel.setBackground(transparent);
            JPanel FilterButtonPanel = new JPanel(null);
            FilterButtonPanel.setPreferredSize(new Dimension(250,1080));
            FilterButtonPanel.setBackground(c1);
            JScrollPane SFilterButtonPanel ;
            SFilterButtonPanel = new JScrollPane(FilterButtonPanel);
            SFilterButtonPanel.setBackground(c1);
            //设置边框
            Border border = BorderFactory.createLineBorder(c2);
            SFilterButtonPanel.setBorder(BorderFactory.createTitledBorder(border,"滤镜选择", TitledBorder.LEFT,
                    TitledBorder.TOP, ti1 ,Color.WHITE));
            SFilterButtonPanel.setHorizontalScrollBar(null);
            SFilterButtonPanel.setHorizontalScrollBarPolicy(
                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            SFilterButtonPanel.getVerticalScrollBar().setUI(new DemoScrollBarUI()); //使用自己设计样式的滑动条
            SFilterButtonPanel.getVerticalScrollBar().setPreferredSize(new Dimension(5, 0));
            SFilterButtonPanel.getVerticalScrollBar().setUnitIncrement(10); //设置滑动条移动速度
//            //设置滑动条监听器，对于样式细节的提升
//            SFilterButtonPanel.getVerticalScrollBar().addMouseListener(new MouseAdapter() {
//                @Override
//                public void mouseEntered(MouseEvent e) {
//                    SFilterButtonPanel.getVerticalScrollBar().setPreferredSize(new Dimension(15, 0));
//                    //SFilterButtonPanel.getVerticalScrollBar().
//                }
//                @Override
//                public  void mouseExited(MouseEvent e){
//                    try {
//                        sleep(1000);
//                    } catch (InterruptedException interruptedException) {
//                        interruptedException.printStackTrace();
//                    }
//                    SFilterButtonPanel.getVerticalScrollBar().setPreferredSize(new Dimension(2, 0));
//                }
//            });
            FilterPanel.add("Center",SFilterButtonPanel);

            //设置里面的滤镜效果图片
            int FilterNum = 8;
            FilterFile.add(new File("src/Icon/Filter/原图.png"));
            FilterFile.add(new File("src/Icon/Filter/冰冻.png"));
            FilterFile.add(new File("src/Icon/Filter/动画.png"));
            FilterFile.add(new File("src/Icon/Filter/浮雕.png"));
            FilterFile.add(new File("src/Icon/Filter/素描.png"));
            FilterFile.add(new File("src/Icon/Filter/连环画.png"));
            FilterFile.add(new File("src/Icon/Filter/阴间.png"));
            FilterFile.add(new File("src/Icon/Filter/颜色变换.png"));
            //添加滤镜Label在Panel中
            for (int i = 0 ; i < FilterNum ; i++){
                JLabel jLabel = new JLabel();
                jLabel.setBounds(15 + ( 100 + 10 ) * (i % 2 ) ,
                        25 + (120 + 10) * (i / 2),100,120);
                FilterLabel.add(jLabel);
                Image im = null;
                try {
                    im = ImageIO.read(FilterFile.get(i));
                } catch (IOException e) {
                    System.out.println("没有读取到滤镜图片");
                }
                im = im.getScaledInstance(100,100,Image.SCALE_DEFAULT);
                jLabel.setVerticalTextPosition(JLabel.BOTTOM);
                jLabel.setHorizontalTextPosition(JLabel.CENTER);
                jLabel.setIcon(new ImageIcon(im));
                jLabel.setFont(ti1);
                jLabel.setForeground(Color.WHITE);
                jLabel.setText(FilterFile.get(i).getName());
                jLabel.setVerticalTextPosition(JLabel.BOTTOM);
                FilterButtonPanel.add(jLabel);
                //激活requestFor方法，使标签可以显示，很重要，没有的话，图片不能显示
                if (i == 0) {
                    jLabel.setDisplayedMnemonic(501);//指定表示助记键的键码
                } else {
                    jLabel.setDisplayedMnemonic(i);
                }
            }
            //添加联合监听器防止，快速点击，图片处理不完，而产生报错
            MouseAdapter e = new MouseAdapter() {
                Object old;
                //防止重复点击
                //加上互斥锁，反之冲突
                @Override
                public synchronized void mouseClicked(MouseEvent e1) {
//                    System.out.println(FilterLabel.size());
                    /*
                    原图
                    冰冻
                    动画
                    浮雕
                    素描
                    连环画
                    阴间
                    颜色变换
                     */
                    if (e1.getSource() == FilterLabel.get(0) && e1.getSource() != old) {
                        showPicture(image);
                    } else if (e1.getSource() == FilterLabel.get(1) && e1.getSource() != old) {
                        FImage = OpencvI.frozen(filepath);
                        showPicture(FImage);
                    } else if (e1.getSource() == FilterLabel.get(2) && e1.getSource() != old) {
                        FImage = OpencvI.Cartoon(filepath);
                        showPicture(FImage);
                    } else if (e1.getSource() == FilterLabel.get(3) && e1.getSource() != old) {
                        FImage = OpencvI.Carving(filepath);
                        showPicture(FImage);
                    } else if (e1.getSource() == FilterLabel.get(4) && e1.getSource() != old) {
                        FImage = OpencvI.Sketch(filepath);
                        showPicture(FImage);
                    } else if (e1.getSource() == FilterLabel.get(5) && e1.getSource() != old) {
                        FImage = OpencvI.Comic(filepath);
                        showPicture(FImage);
                    } else if (e1.getSource() == FilterLabel.get(6) && e1.getSource() != old) {
                        FImage = OpencvI.Underworld(filepath);
                        showPicture(FImage);
                    } else if (e1.getSource() == FilterLabel.get(7) && e1.getSource() != old) {
                        FImage = OpencvI.ColorChange(filepath);
                        showPicture(FImage);
                    }
                    old = e1.getSource();
                }
            };
            //添加监听器
            for (int i = 0;i < FilterNum ;i ++){
                FilterLabel.get(i).addMouseListener(e);
            }
        }
    }
    /** 多线程处理面板  截切面板初始化 */
    class IntiCutPanel extends Thread{
        public void run(){
            cutPanel.setBackground(c1);
            JPanel cutButtonPanel = new JPanel();
            cutButtonPanel.setPreferredSize(new Dimension(250,1080));
            cutButtonPanel.setBackground(transparent);

            //开始部署功能
            /*
            FlipFilter FlipFilter对图像进行简单的反射和旋转。
            您可以将图像旋转90度、180度或270度，水平、垂直或围绕其主对角线翻转图像。
             */
            FlipFilter flip = new FlipFilter();

            /*
            MirrorFilter 镜像效果函数
             */
            MirrorFilter Mirror = new MirrorFilter();
            flipButton Flip = new flipButton("翻转");
            flipButton rotate = new flipButton("旋转");
            cutButtonPanel.add(rotate);
            cutButtonPanel.add(Flip);

//            ActionListener event = new ActionListener(){
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    if (e.getSource() == Flip){
//                        System.out.println("1");
//                        flip.setOperation(4);
//                        flip.filter((BufferedImage) SImage,(BufferedImage)SImage);
//                        showPicture(SImage);
//                    }else if (e.getSource() == rotate){
//
//                    }
//                }
//            };
            Flip.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    flip.setOperation(1);
                    flip.filter((BufferedImage) SImage,(BufferedImage)SImage);
                    showPicture(SImage);
                }
            });
            rotate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    flip.setOperation(4);
                    BufferedImage s = ImageFilters.createRotateBufferedImage((BufferedImage) SImage);
                    flip.filter((BufferedImage) SImage,s);
                    showPicture(s);
                }
            });
            cutPanel.add(cutButtonPanel);
        }
    }

    class flipButton extends JButton {
        public flipButton(String str){
            super(str);
            setBackground(new Color(85,85,85));
            setFocusPainted(false);
            setPreferredSize(new Dimension(120,50));
            setBorderPainted(false);
            setFont(ti);
            setForeground(Color.WHITE);
        }
    }


    /** 多线程处理面板  调整面板初始化 */
    class IntiAdjustmentPanel extends Thread{
        public void run(){
            AImage = ImageFilters.copyBufferedImage((BufferedImage) SImage);

            //调整面板的设计
            adjustmentPanel.setBackground(transparent);
            JPanel adjustmentSliderPanel = new JPanel();
            JScrollPane SAdjustmentSliderPanel ;
            adjustmentSliderPanel.setPreferredSize(new Dimension(250,1080));
            SAdjustmentSliderPanel = new JScrollPane(adjustmentSliderPanel);
            adjustmentSliderPanel.setBackground(c1);
            SAdjustmentSliderPanel.setBackground(c1);
            //设置边框
            Border border = BorderFactory.createLineBorder(c1);
            SAdjustmentSliderPanel.setBorder(border);
            SAdjustmentSliderPanel.setHorizontalScrollBar(null);

            SAdjustmentSliderPanel.getVerticalScrollBar().setUI(new DemoScrollBarUI()); //使用自己设计样式的滑动条
            SAdjustmentSliderPanel.getVerticalScrollBar().setPreferredSize(new Dimension(5, 0));
            SAdjustmentSliderPanel.getVerticalScrollBar().setUnitIncrement(10); //设置滑动条移动速度

            JLabel text1 = new JLabel("光线");

            text1.setForeground(Color.WHITE);
            text1.setFont(ti);
            adjustmentSliderPanel.add(text1);

            //填充面板
            JPanel block = new JPanel();
            block.setPreferredSize(new Dimension(140,50));
            block.setBackground(transparent);
            adjustmentSliderPanel.add(block);

            JLabel back = new JLabel("重置");
            back.setForeground(Color.WHITE);
            back.setFont(ti1);
            adjustmentSliderPanel.add(back);

            SliderPanel contrastRatioPanel = new SliderPanel("对比度");
            SliderPanel ExposurePanel = new SliderPanel("曝光度");
            SliderPanel tonePanel = new SliderPanel("色 调");
            SliderPanel saturationPanel = new SliderPanel("饱和度");
            SliderPanel brightnessPanel = new SliderPanel("亮 度");
            SliderPanel LevelsFilter = new SliderPanel("水平度——低");
            SliderPanel HLevelsFilter = new SliderPanel("水平度——高");
            SliderPanel RFilter = new SliderPanel("红 色");
            SliderPanel GFilter = new SliderPanel("绿 色");
            SliderPanel BFilter = new SliderPanel("蓝 色");

            //设置重置按钮
            back.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    FilterImage.setImage((BufferedImage) AImage);
                    contrastRatioPanel.contrastRatio.setValue(0);
                    ExposurePanel.contrastRatio.setValue(0);
                    tonePanel.contrastRatio.setValue(0);
                    saturationPanel.contrastRatio.setValue(0);
                    brightnessPanel.contrastRatio.setValue(0);
                    LevelsFilter.contrastRatio.setValue(0);
                    HLevelsFilter.contrastRatio.setValue(0);
                    RFilter.contrastRatio.setValue(0);
                    GFilter.contrastRatio.setValue(0);
                    BFilter.contrastRatio.setValue(0);
                    showPicture(AImage);
                }
            });

            //添加面板
            adjustmentSliderPanel.add(contrastRatioPanel);
            adjustmentSliderPanel.add(ExposurePanel);
            adjustmentSliderPanel.add(tonePanel);
            adjustmentSliderPanel.add(saturationPanel);
            adjustmentSliderPanel.add(brightnessPanel);
            adjustmentSliderPanel.add(LevelsFilter);
            adjustmentSliderPanel.add(HLevelsFilter);
            //填充面板
            JPanel block1 = new JPanel();
            block1.setPreferredSize(new Dimension(220,50));
            block1.setBackground(transparent);
            adjustmentSliderPanel.add(block1);

            //设置颜色模板区域
            JLabel text2 = new JLabel("颜色");
            text2.setForeground(Color.WHITE);
            text2.setFont(ti);
            adjustmentSliderPanel.add(text2);

            //填充面板
            JPanel block3 = new JPanel();
            block3.setPreferredSize(new Dimension(180,50));
            block3.setBackground(transparent);
            adjustmentSliderPanel.add(block3);

            //添加面板
            adjustmentSliderPanel.add(RFilter);
            adjustmentSliderPanel.add(GFilter);
            adjustmentSliderPanel.add(BFilter);


            //设置显示参数界面
            JWindow text = new JWindow();
            text.setShape(new RoundRectangle2D.Double(0, 0, 30, 20, 5, 5));
            text.setSize(new Dimension(30,20));
            text.setLayout(new FlowLayout());
            text.setBackground(new Color(255,255,255,70));
            JLabel num = new JLabel();
            num.setForeground(Color.WHITE);
            text.add(num);
            text.setVisible(false);
            //鼠标拖动显示滑块的值
            MouseMotionAdapter e1 = new MouseMotionAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {
                    //按住时候显示数值
                    num.setText(String.valueOf(((JSlider)e.getSource()).getValue()));
                    //System.out.println(num.getText());

                    text.setLocation(e.getXOnScreen(),e.getYOnScreen() - 30);
                    text.setVisible(true);
                }
            };
            //鼠标离开不显示当前滑块的值
            MouseAdapter e2 = new MouseAdapter() {
                @Override
                public void mouseExited(MouseEvent e) {
                    super.mouseExited(e);
                    text.setVisible(false);
                }
            };
            contrastRatioPanel.contrastRatio.addMouseMotionListener(e1);
            contrastRatioPanel.contrastRatio.addMouseListener(e2);
            ExposurePanel .contrastRatio.addMouseMotionListener(e1);
            ExposurePanel.contrastRatio.addMouseListener(e2);
            tonePanel.contrastRatio.addMouseMotionListener(e1);
            tonePanel.contrastRatio.addMouseListener(e2);
            saturationPanel.contrastRatio.addMouseMotionListener(e1);
            saturationPanel.contrastRatio.addMouseListener(e2);
            brightnessPanel.contrastRatio.addMouseMotionListener(e1);
            brightnessPanel.contrastRatio.addMouseListener(e2);
            LevelsFilter.contrastRatio.addMouseMotionListener(e1);
            LevelsFilter.contrastRatio.addMouseListener(e2);
            HLevelsFilter.contrastRatio.addMouseMotionListener(e1);
            HLevelsFilter.contrastRatio.addMouseListener(e2);
            RFilter.contrastRatio.addMouseMotionListener(e1);
            RFilter.contrastRatio.addMouseListener(e2);
            GFilter.contrastRatio.addMouseMotionListener(e1);
            GFilter.contrastRatio.addMouseListener(e2);
            BFilter.contrastRatio.addMouseMotionListener(e1);
            BFilter.contrastRatio.addMouseListener(e2);



            //注意此除还要在按钮设置一下
            FilterImage = new ImageFilters(AImage);
            FilterImage.setImage((BufferedImage) AImage);
            //传入参数到图片处理函数中
            ChangeListener e = e3 -> {
                if (e3.getSource() == contrastRatioPanel.contrastRatio){
                    contrast = (float) ((contrastRatioPanel.contrastRatio.getValue() / 100.0 ) + 1);
                }else if (e3.getSource() == ExposurePanel.contrastRatio){
                    Exposure = (float) ((ExposurePanel.contrastRatio.getValue() / 100.0 ));
                    if (Exposure > 0)
                        Exposure = (Exposure * 3 + 1);
                    else
                        Exposure += 1;
                    //System.out.println(Exposure);
                }else if (e3.getSource() == tonePanel.contrastRatio){
                    tone = (float) ((tonePanel.contrastRatio.getValue() / 100.0 ));
                }else if (e3.getSource() == saturationPanel.contrastRatio){
                    saturation = (float) saturationPanel.contrastRatio.getValue();

                }else if (e3.getSource() == brightnessPanel.contrastRatio){
                    brightness = (float) ((brightnessPanel.contrastRatio.getValue() / 100.0 ) * 0.5 + 1);

                }else if (e3.getSource() == LevelsFilter.contrastRatio){
                    levels = (float) ((LevelsFilter.contrastRatio.getValue() / 100.0 ) * 0.9 );

                }else if (e3.getSource() == HLevelsFilter.contrastRatio){
                    Hlevels = (float) (((HLevelsFilter.contrastRatio.getValue() / 100.0 ) * 0.9 + 1));
                }else if (e3.getSource() == RFilter.contrastRatio){
                    R = (float) (((RFilter.contrastRatio.getValue() / 100.0 ) * 3));
                }else if (e3.getSource() == GFilter.contrastRatio){
                    G = (float) (((GFilter.contrastRatio.getValue() / 100.0 ) * 3));
                }else if (e3.getSource() == BFilter.contrastRatio){
                    B = (float) (((BFilter.contrastRatio.getValue() / 100.0 ) * 3));
                }
                S = FilterImage.filter(AImage,contrast,Exposure,tone,saturation,brightness,levels,Hlevels,
                        R,G,B);
                //集体处理所有的图片参数，防止各个参数冲突导致最后图片成像不理想
                showPicture(S);
            };
            contrastRatioPanel.contrastRatio.addChangeListener(e);
            ExposurePanel.contrastRatio.addChangeListener(e);
            tonePanel.contrastRatio.addChangeListener(e);
            saturationPanel.contrastRatio.addChangeListener(e);
            brightnessPanel.contrastRatio.addChangeListener(e);
            LevelsFilter.contrastRatio.addChangeListener(e);
            HLevelsFilter.contrastRatio.addChangeListener(e);
            RFilter.contrastRatio.addChangeListener(e);
            GFilter.contrastRatio.addChangeListener(e);
            BFilter.contrastRatio.addChangeListener(e);
            adjustmentPanel.add(SAdjustmentSliderPanel);
        }
    }
    //调正参数数据
    /*参数对应
            contrastRatioPanel    contrast
            ExposurePanel         Exposure
            tonePanel             tone
            saturationPanel       saturation
            brightnessPanel       brightness
            LevelsFilter          levels
            HLevelsFilter         Hlevels
            RFilter               R
            GFilter               G
            BFilter               B
    如下，开始初始化数据
    */
    float contrast = 1, Exposure = 1, tone = 0, saturation = 1, brightness = 1, levels = 0,Hlevels = 1,
            R = 0, G = 0, B = 0;
    Image S = image;
    ImageFilters FilterImage;


    class SliderPanel extends JPanel{
        public JSlider contrastRatio;
        public SliderPanel(String name){
            contrastRatio = new JSlider(-100,100);
            contrastRatio.setFocusable(false);
            setPreferredSize(new Dimension(220,60));
            setBackground(transparent);
            contrastRatio.setUI(new DemoSliderBarUI(contrastRatio));
            contrastRatio.setBorder(null);
            contrastRatio.setBackground(c1);
            add(contrastRatio);
            Border border = BorderFactory.createLineBorder(c2);
            setBorder(BorderFactory.createTitledBorder(border,name, TitledBorder.LEFT,
                    TitledBorder.TOP, ti1 ,Color.WHITE));
        }
    }

    /** 监听器初始化 */
    public void InitListener(){
        /*
        Filter
        cut
        adjustment 按钮初始化
         */
        Filter.setBackground(new Color(63,63,63));
        ActionListener e = e1 -> {
            if (e1.getSource() == Filter){
                Name.setText("滤镜");
                card.show(CardPanel,"1");
                FImage = SImage;
            }else if (e1.getSource() == cut){
                Name.setText("剪切");
                card.show(CardPanel,"0");

            }else if (e1.getSource() == adjustment){
                AImage = SImage;
                FilterImage.setImage((BufferedImage) AImage);
                Name.setText("调整");
                card.show(CardPanel,"2");
            }
        };
        //设置鼠标移动特效,点击特效等
        MouseAdapter e1 = new MouseAdapter() {
            Component now = Filter;
            @Override
            public void mouseEntered(MouseEvent e) {
                e.getComponent().setBackground(new Color(56, 56, 56));
            }
            @Override
            public void mouseExited(MouseEvent e) {
                if (now != e.getComponent())
                    e.getComponent().setBackground(new Color(34,34,34));
            }
            @Override
            public void mousePressed(MouseEvent e) {
                e.getComponent().setBackground(new Color(83, 83, 83));
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                e.getComponent().setBackground(new Color(63,63,63));
                if (now != e.getComponent())
                    now.setBackground(c2);
                now = e.getComponent();
            }
        };
        Filter.addMouseListener(e1);
        cut.addMouseListener(e1);
        adjustment.addMouseListener(e1);
        Filter.addActionListener(e);
        cut.addActionListener(e);
        adjustment.addActionListener(e);

        /*
        KeepB 保存
        cancel 取消
         */

        KeepB.addActionListener(e2 ->{
                    // 创建一个默认的文件选取器
                    JFileChooser fileChooser = new JFileChooser();

                    // 设置打开文件选择框后默认输入的文件名
                    fileChooser.setSelectedFile(new File("副本.jpg"));
                    // 设置默认使用的文件过滤器
                    fileChooser.setFileFilter(new FileNameExtensionFilter("图像文件（JPG/GIF）",
                            "JPG", "JPEG", "GIF"));
                    // 打开文件选择框（线程将被阻塞, 直到选择框被关闭）
                    int result = fileChooser.showSaveDialog(this);

                    if (result == JFileChooser.APPROVE_OPTION) {
                        // 如果点击了"保存", 则获取选择的保存路径
                        File file = fileChooser.getSelectedFile();
                        try {
                            ImageIO.write((RenderedImage) SImage, "jpg", fileChooser.getSelectedFile());//写入文件
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }
            }
        );
        cancel.addActionListener(e2 -> {
            dispose();
        });

    }
    /** 图片展示窗口 */
    public void showPicture(Image image){

        this.SImage = image;
        ImageIcon i;
        //使用窗口代码
        int HImage = image.getHeight(null);
        int WImage = image.getWidth(null);
        double w1,h1;
        if ((double) HImage/WImage > (double) FH/FW){
            w1 = (double) WImage / HImage * FH;
            h1 = FH;
        }
        else{
            w1 = FW;
            h1 = (double) HImage / WImage * FW;
        }
        //每次变化，填充面板也跟着变化
        TPanel.setPreferredSize(new Dimension((FW + 80 - (int)w1)/2,100));
        //System.out.println(w1 + " " +h1 );
        i = new ImageIcon(image.getScaledInstance((int)w1, (int)h1, Image.SCALE_DEFAULT));
        ImageLabel.setIcon(i);
    }

    //图片复制
    public BufferedImage createBufferedImage(BufferedImage src) {
        ColorModel cm = src.getColorModel();
        BufferedImage image = new BufferedImage(
                cm,
                cm.createCompatibleWritableRaster(src.getWidth(), src.getHeight()),
                cm.isAlphaPremultiplied(),
                null);
        return image;
    }
    public static void main(String[] args) {
        Image i = null;
        try {
            i = ImageIO.read(new File("C:\\Users\\22806\\OneDrive\\图片\\1.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        new CreateUI(i,"C:\\Users\\22806\\OneDrive\\图片\\1.jpg");
    }
}
