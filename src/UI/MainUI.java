package UI;
/**
 * @Author:pengshi
 * @Version:[v1.0]
 * 主界面UI的类
 */
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Objects;


import ComponentTool.jButton;
import MainuiTool.*;
import test.SafeBoxUI;

//主要界面UI
public class MainUI extends JFrame{

    public static MainUI Main;

    //保险箱
    public SafeBoxUI safeBoxUI;


    //菜单栏参数
    public JMenuBar bar = new JMenuBar();
    public jButton Collection = new jButton("主页面");
    public jButton FilePicture = new jButton("保险箱");
    public jButton Input = new jButton("展示");
    public jButton Election = new jButton("关于");

    //滚动目录树
    final JTree jTree = new JTree();
    public String filepath;//当前文件路径

    //左边目录树变量
    public JScrollPane ProjectPanel;// 目录树的面板
    FileNode ParentNode;//访问文件夹的地址

    //右边预览面板变量
    public JPanel PicturePanel = new JPanel();//图片显示面板
    public JScrollPane SPicturePanel ;//设置滚动面板
    public ArrayList<JPanel> ImagesPanel = new ArrayList<>();//预览框内的图片介绍面板
    public ArrayList<JLabel> ImagesLabel = new ArrayList<>();//预览框下的缩略图片
    public ArrayList<File> ImagesFile = new ArrayList<>();//预览图片加载
    public ArrayList<JTextField> ImagesField = new ArrayList<>();//预览框下的文字
    public ArrayList<File> ClickedFilePath = new ArrayList<>(); // 鼠标点击的文件路径下的所有文件
    public JPanel FileInformationPanel = new JPanel();//显示图片总数和，文件地址
    public JPanel PictureNum = new JPanel();//显示图片总数的空间文字区域
    public JLabel File_name = new JLabel();
    public JLabel File_name1 = new JLabel();
    public JLabel File_name2 = new JLabel();
    Graphics2D g2;//画笔

    //左下角展示面板
    public JPanel LeftLowerPanel = new JPanel();

    //矩形选框
    public Rectangle rect;//设计鼠标函数选框
    public JLabel ImageNum = new JLabel();
    Point pressPoint = null, releasePoint = null;
    ArrayList<Point> points = new ArrayList<>();
    public Point pBegin;
    ArrayList<Integer> SelectImages = new ArrayList<>();
    double x1, y1, x2, y2;

    //右击菜单栏
    PopupMenuTool PopupMenu = new PopupMenuTool(); // 右键单击文件时弹出的弹出式菜单

    //测试面板
//    JPanel test = new JPanel();

    //程序入口
    public static void main(String[] args) {
        Main = new MainUI();
    }

    //初始化操作
    public MainUI(){

        setTitle("图享 1.0");
        Image image = null;
        try {
            image = ImageIO.read(new File("src/Icon/灰图库.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setIconImage(image);
        setLayout(null);//设置为流式布局
        setResizable(false);//设置不要缩放
        setBackground(Color.WHITE);
        getContentPane().setBackground(Color.white);
        setSize(1000,650);
        setLocationRelativeTo(null);
        //尝试使用系统UI界面设置
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            System.out.println(" ");
        } catch (IllegalAccessException e) {
            System.out.println(" 1");
        } catch (UnsupportedLookAndFeelException e) {
            System.out.println(" 2");
        }

        //开始设置头部菜单栏
        //主页面菜单栏
        bar.setBorderPainted(false);
        bar.setBackground(Color.white);
        bar.add(Collection);
        bar.add(FilePicture);
        bar.add(Input);
        bar.add(Election);
        setJMenuBar(bar);




        //按钮监听事件
        FilePicture.addActionListener(e -> {
            JFrame jFrame = new JFrame();
            jFrame.setLayout(new BorderLayout());
            jFrame.setSize(400,100);
            jFrame.setTitle("输入密码");
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
            jFrame.setResizable(false);
            JPanel jpanel = new JPanel();
            final JPasswordField textField = new JPasswordField(10);
            textField.setFont(new Font(null, Font.PLAIN, 20));
            jpanel.add(textField);
            JButton btn = new JButton("登陆");
            btn.setFont(new Font(null, Font.PLAIN, 20));
            jpanel.add(btn);
            jFrame.setContentPane(jpanel);
            btn.addActionListener(e1 -> {
                if (new String(textField.getPassword()).equals("8888")) {
                    try {
                        jFrame.dispose();
                        safeBoxUI.setVisible(true);
                    } catch (Exception eve){
                        System.out.println("界面还在加载中");
                    }
                }
            });
            jFrame.setVisible(true);
        });


        Input.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ImagesFile.size() > 0)
                    ShowUI(0);
                else
                    JOptionPane.showMessageDialog(null, "该文件夹下没有图片",
                            "图片展示界面提示",JOptionPane.WARNING_MESSAGE);
            }
        });


        //关于按钮信息
        Election.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(Main,"软件名字：图享1.0\n" +
                        "制作者：彭川、杨俊杰、温汉勇（软件工程4班）\n" +
                        "该软件使用鼠标和键盘进行操作，可以对于图片进行相关操作\n"+
                        "希望用户可以放心使用\n"
                        ,"关于",JOptionPane.PLAIN_MESSAGE);
            }
        });



        //框架面板container
        Container container = getContentPane();

        //目录树建立
        initJTree(jTree);
        ProjectPanel = new JScrollPane(jTree);
        ProjectPanel.setBounds(5,5,200,530);

        ProjectPanel.getViewport().setBackground(Color.white);//注意JScrollPanel的结构，view是用来显示的面板
        ProjectPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        container.add(ProjectPanel);

        //右侧的图片显示面板
        //设置文件夹中文件数量显示
        FileInformationPanel.setLayout(null);
        FileInformationPanel.setBounds(210,5,770,50);
        FileInformationPanel.setBackground(Color.white);
        FileInformationPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        PictureNum.setBounds(20,1,300,48);//注意770长度使用了300的空间
        PictureNum.setBackground(Color.WHITE);
        PictureNum.setLayout(null);
        File_name.setBounds(0,0,48,48);
        PictureNum.add(File_name);
        File_name1.setBounds(48,5,120,20);
        PictureNum.add(File_name1);
        File_name2.setBounds(48,25,48,20);
        PictureNum.add(File_name2);
        FileInformationPanel.add(PictureNum);
        PicturePanel.setBackground(Color.white);

        PicturePanel.setLocation(0,0);
        PicturePanel.setPreferredSize(new Dimension(770,450));
        SPicturePanel = new JScrollPane(PicturePanel);
        SPicturePanel.setHorizontalScrollBar(null);

//        System.out.println(PicturePanel.getWidth() + " " + PicturePanel.getHeight());
        SPicturePanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        SPicturePanel.setBounds(210,60,770,500);
        add(FileInformationPanel);
        add(SPicturePanel);

        //setOpacity(0);

        //左下角展示面板
        LeftLowerPanel.setLayout(new FlowLayout());
        LeftLowerPanel.setBounds(5,540,200,30);
        LeftLowerPanel.add(ImageNum);
        LeftLowerPanel.setBackground(Color.white);
        LeftLowerPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        add(LeftLowerPanel);
        ImageNum.setText("共"+0+"张("+"0.00MB"+")-"+"选中图片数目：" + 0);

        //test
//        test.setBounds(5,540,200,30);
//        test.setBackground(Color.blue);
//        add(test);
        WelcomeWindow.start();

        //右击点击事件菜单
        setVisible(true);
        safeBoxUI = new SafeBoxUI();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE) ;
    }

    //初始化目录树的设置
    public void initJTree(JTree jTree1){
        File[] roots = (new PFileSystemView()).getRoots();// 创建具有默认文件夹名称的新文件夹并返回此系统上的所有根分区
        FileNode nod;
        nod = new FileNode(roots[0]);
        nod.explore();
        jTree1.setModel(new DefaultTreeModel(nod));// 设置将提供数据的TreeModel
        jTree1.addTreeExpansionListener(new TreeExpansionListener() {// 为 TreeExpansion事件添加侦听 TreeExpansion

            public void treeExpanded(TreeExpansionEvent event) {
                TreePath path = event.getPath();// 获取当前路径
                ParentNode = (FileNode) path.getLastPathComponent();// 返回此路径的最后一个元素
                if (ParentNode.toString().equals("网络")) {
                    //测试出bug对于电脑中 网络 这个应用不能点开，会无限死循环
                    ParentNode.explored_ = true ;
                }
                else if (!ParentNode.isExplored()) {
                    //查看是否有被开发
                    DefaultTreeModel model = ((DefaultTreeModel) jTree1.getModel());
                    ParentNode.explore();
                    model.nodeStructureChanged(ParentNode);// 发布treeStructureChanged事件
                }
            }

            public void treeCollapsed(TreeExpansionEvent event) {
            }
        });

        jTree1.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                ImagesShow(e); // 图片预览
            }
        });
    }
    boolean haveFlag = false;//查看是不是重复访问了


    //图片预览窗口
    private synchronized void ImagesShow(MouseEvent e) {

        JTree tree = (JTree) e.getSource();// 鼠标事件的事件源
        int row = tree.getRowForLocation(e.getX(), e.getY());// 返回指定位置的行
        if (row == -1) {
            return;// 该位置不在显示的单元格的范围内
        }
        TreePath path = tree.getPathForRow(row);// 返回指定行的路径

        if (path == null) {
            return;// row < 0或 row >= getRowCount()
        }
        FileNode node = (FileNode) path.getLastPathComponent();// 返回此路径的最后一个元素
        if (node == null) {
            return;
        }

        //访问开关，防止重复访问，产生报错
        if (filepath == null || !filepath.equals(node.getString())){
            haveFlag = false;
            filepath = node.getString();//获取当前文件夹
        }
        else{
            haveFlag = true;
        }

        //System.out.println(filepath);
        if (!haveFlag)
            Show(node.getWorR());
    }

    Thread t1;
    Thread t2;
    Thread t3;
    Thread t4;
    Thread t5;
    Thread t6;
    //图片展示模块
    public synchronized void Show(File url){

        //      判断进程是不是曾经运行过，有的话中断，重新分配运行
        if ( t1 != null ){
            t1.interrupt();
            t2.interrupt();
            t3.interrupt();
            t4.interrupt();
            t5.interrupt();
            t6.interrupt();
        }
        //初始化变量
        ImagesFile.clear();
        ImagesField.clear();
        ImagesLabel.clear();
        ImagesPanel.clear();
        ClickedFilePath.clear();
        PicturePanel.removeAll();
        PicturePanel.repaint();
        PicturePanel.setLayout(null);
        File[] files = url.listFiles();// 此路径名的所有文件

        if (files == null) return;
        int PictureNumber = 0;
        int i;
        for (i = 0; i < Objects.requireNonNull(files).length; i++) {
            String fileName = files[i].getName();
            String format = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            //开始查看文件格式程序能够显示的图片格式包括，.JPG、.JPEG、.GIF、.PNG、和.BMP。
            if (format.equals("jpg") || format.equals("gif") || format.equals("bmp") || format.equals("png")|| format.equals("jpeg")) {
                PictureNumber++;
                ImagesFile.add(files[i]);// 进入图像文件队列
            }
        }
        if (PictureNumber == 0) return;
        for (int m = 0; m < ImagesFile.size(); m++) {
            // 创建预览图片
            ImagesLabel.add(new JLabel());
            ImagesPanel.add(new JPanel());
            ImagesField.add(new JTextField());
        }

        for (i = 0; i < ImagesField.size(); i++) {
            //设置白色背景
            ImagesPanel.get(i).setBackground(Color.WHITE);
            ImagesLabel.get(i).setBackground(Color.white);
            ImagesField.get(i).setBackground(Color.WHITE);
        }

        int sum = ImagesFile .size();
        //System.out.println(PictureNumber);
        //计算图片量所致的面板大小
        if (PictureNumber / 5 >= 3){
            SPicturePanel.getVerticalScrollBar().setVisible(true);// 将此滚动条的值设置为指定的值
            PicturePanel.setPreferredSize(new Dimension(770,505+130*(PictureNumber/5 - 2)+ 75));
            SPicturePanel.getVerticalScrollBar().setUnitIncrement(20);//设置滚动条速度
        } else {
            PicturePanel.setPreferredSize(new Dimension(770,450));
            SPicturePanel.getVerticalScrollBar().setValue(0);// 将此滚动条的值设置为指定的值
            SPicturePanel.getVerticalScrollBar().setVisible(false);// 将此滚动条的值设置为指定的值
        }



        // 多线程处理图片
        Runnable threadImages1 = new ThreadImages(ImagesFile, 0, sum, ImagesLabel , ImagesField , ImagesPanel ,
                PicturePanel );
        Runnable threadImages2 = new ThreadImages(ImagesFile, 1 ,   sum, ImagesLabel , ImagesField ,
                ImagesPanel , PicturePanel );
        Runnable threadImages3 = new ThreadImages(ImagesFile, 2,  sum, ImagesLabel , ImagesField ,
                ImagesPanel , PicturePanel );
        Runnable threadImages4 = new ThreadImages(ImagesFile,  3,  sum, ImagesLabel , ImagesField ,
                ImagesPanel , PicturePanel );
        Runnable threadImages5 = new ThreadImages(ImagesFile,  4,  sum, ImagesLabel , ImagesField ,
                ImagesPanel , PicturePanel );
        Runnable threadImages6 = new ThreadImages(ImagesFile,  5, sum, ImagesLabel , ImagesField ,
                ImagesPanel , PicturePanel );
         t1 = new Thread(threadImages1);
         t2 = new Thread(threadImages2);
         t3 = new Thread(threadImages3);
         t4 = new Thread(threadImages4);
         t5 = new Thread(threadImages5);
         t6 = new Thread(threadImages6);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        //System.out.println("ok");
        //左下角面板设置
        double Storage = 0;
        for (int a = 0;a < ImagesPanel.size();a++){
            Storage += ImagesFile.get(a).length();
        }
        ImageNum.setText("共" + ImagesPanel.size() + "张("+String.format("%.2f",Storage/1024/1024)+"MB)"+"-" +
                "选中图片数目：" + 0);

        //设置显示该目录中共有多少张图片的面板
        Font f1= new Font("微软雅黑",Font.BOLD,18);
        Font f2= new Font("微软雅黑",Font.PLAIN,12);
        Icon icon=new ImageIcon("src/Icon/文件夹.png");
        File_name.setIcon(icon);
        File_name1.setFont(f1);
        File_name1.setText(url.getName());
        File_name2.setFont(f2);
        File_name2.setText(PictureNumber+"张图片");
        InitMouseListener();
    }


    /** 跳转到图片展示界面 */
    public void ShowUI(int choose){
        //ImageUI();
        ImagesShowUI show = new ImagesShowUI(ImagesFile,choose,filepath);
        show.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                //窗口关闭时候，再刷新一遍
                Show(new File(filepath));
            }
        });
    }
    // 创建面板监听器
    public void InitMouseListener() {
        //添加单个图片面板的点击事件

        for (int i = 0; i < ImagesPanel.size(); i++) {
            //鼠标单击事件用图片进行下一步操作。你。
            ImagesPanel.get(i).addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.isControlDown()){
                        //ctrl+鼠标进行选定图片。
                        if (e.getButton() == MouseEvent.BUTTON1){
                            ((JPanel)e.getSource()).setBackground(new Color(204, 232, 255));
                            (((JPanel)e.getSource()).getComponent(1)).setBackground(new Color(204, 232, 255));
                            SelectImages.add(ImagesPanel.indexOf((JPanel)e.getSource()));
                            //System.out.println(SelectImages);
                        }
                        NumShow();
                    }
                    else if (e.getButton() == MouseEvent.BUTTON1){
                        //鼠标单击事件选定图片。
                        SelectImages.clear();
                        for (int b = 0; b < ImagesPanel.size(); b++) {
                            ImagesPanel.get(b).setBackground(Color.white);
                            ImagesField.get(b).setBackground(Color.white);
                        }
                        ((JPanel)e.getSource()).setBackground(new Color(204, 232, 255));
                        (((JPanel)e.getSource()).getComponent(1)).setBackground(new Color(204, 232, 255));
                        SelectImages.add(ImagesPanel.indexOf((JPanel) e.getSource()));
                        NumShow();
                        //System.out.println(SelectImages);
                    }
                    else if (e.getButton() == MouseEvent.BUTTON3) { // 判断鼠标的点击是否为右键的点击
                        // 弹出式菜单在此时弹出，并设置好了其弹出的位置
                        //设置右键点击事件
                        int a = ImagesPanel.indexOf((JPanel) e.getSource());//判断选中右键的面板相对于图片面板的索引
                        if (!SelectImages.contains(a))
                        {
                            SelectImages.clear();
                            for (int b = 0; b < ImagesPanel.size(); b++) {
                                ImagesPanel.get(b).setBackground(Color.white);
                                ImagesField.get(b).setBackground(Color.white);
                            }
                            ((JPanel)e.getSource()).setBackground(new Color(204, 232, 255));
                            (((JPanel)e.getSource()).getComponent(1)).setBackground(new Color(204, 232, 255));
                            SelectImages.add(a);
                            NumShow();
                        }
                        PopupMenu.OutPopupMenu(e,ImagesFile,ImagesLabel,ImagesField,ImagesPanel, SelectImages,
                                PicturePanel,filepath);
                    }
                }
                @Override
                public void mouseClicked(MouseEvent e){
                    int i = e.getClickCount();
                    if(i == 2){
                        //另一个主窗口入口ImageUI
                        ShowUI(SelectImages.get(0));
                    }
                }
            });
        }
        //添加图片面板的鼠标点击事件
        PicturePanel.addMouseMotionListener(new MouseMotionAdapter() {
            //绘制鼠标拖动矩形选框
            public void mouseDragged(MouseEvent evt) {
                try {
                    g2 = (Graphics2D) PicturePanel.getGraphics();
                    g2.setXORMode(Color.white);
                    if (rect != null) {
                        g2.draw(rect);
                    }
                    // 绘制现在的矩形
                    rect = new Rectangle((int) Math.min(pBegin.getX(), evt.getPoint().getX()),
                            (int) Math.min(pBegin.getY(), evt.getPoint().getY()),
                            (int) Math.abs(pBegin.getX() - evt.getPoint().getX()),
                            (int) Math.abs(pBegin.getY() - evt.getPoint().getY()));
                    g2.draw(rect);
                    g2.setPaintMode();
                    g2.dispose();
                }catch (Exception e){
                    System.out.println("无法绘制");
                }
            }
        });
        PicturePanel.addMouseListener(new MouseAdapter() {

            public void mouseReleased(MouseEvent e) {
                try {
                    // 画选择框（实际效果为去掉）
                    g2 = (Graphics2D) PicturePanel.getGraphics();
                    g2.setColor(Color.black);
                    g2.setXORMode(Color.white);
                    g2.draw(rect);
                    g2.setPaintMode();
                    g2.dispose();
                } catch (Exception event) {
                    //System.out.println("无法除去画笔轨迹");
                }
                try{
                    pBegin = null;
                    rect = null;
                    releasePoint = new Point(e.getLocationOnScreen());
                    if (releasePoint.getX() > pressPoint.getX()) {
                        x2 = releasePoint.getX();
                        x1 = pressPoint.getX();
                    } else {
                        x1 = releasePoint.getX();
                        x2 = pressPoint.getX();
                    }
                    if (releasePoint.getY() > pressPoint.getY()) {
                        y2 = releasePoint.getY();
                        y1 = pressPoint.getY();
                    } else {
                        y1 = releasePoint.getY();
                        y2 = pressPoint.getY();
                    }
                }catch (Exception event){
                    System.out.println("无法进行坐标定位");
                }
                //System.out.println(x1+" " +x2+" "+y1+" "+y2);
                SelectImages.clear();//记住提前清理
                for (int b = 0; b < ImagesPanel.size(); b++) {
                    ImagesPanel.get(b).setBackground(Color.white);
                    ImagesField.get(b).setBackground(Color.white);
                }
                double Storage = 0;
                for (int i = 0; i < ImagesPanel.size(); i++) {
                    if (((ImagesPanel.get(i).getLocationOnScreen().getX() + 120 <= x2
                            && ImagesPanel.get(i).getLocationOnScreen().getX() >= x1)
                            || (ImagesPanel.get(i).getLocationOnScreen().getX() + 120 >= x2
                            && ImagesPanel.get(i).getLocationOnScreen().getX() <= x2)
                            || (ImagesPanel.get(i).getLocationOnScreen().getX() + 120 >= x1
                            && ImagesPanel.get(i).getLocationOnScreen().getX() <= x1))
                    && ((ImagesPanel.get(i).getLocationOnScreen().getY() >= y1
                            && ImagesPanel.get(i).getLocationOnScreen().getY() + 130 <= y2)
                            || (ImagesPanel.get(i).getLocationOnScreen().getY() + 130 >= y2
                            && ImagesPanel.get(i).getLocationOnScreen().getY() <= y2)
                            || (ImagesPanel.get(i).getLocationOnScreen().getY() + 130 >= y1
                            && ImagesPanel.get(i).getLocationOnScreen().getY() <= y1))) {
                        ImagesPanel.get(i).setBackground(new Color(204, 232, 255));
                        ImagesField.get(i).setBackground(new Color(204, 232, 255));
                        SelectImages.add(i);
                    }
                    Storage += ImagesFile.get(i).length();
                }
                ImageNum.setText("共" + ImagesPanel.size() + "张("+String.format("%.2f",Storage/1024/1024)+"MB)"+"-" +
                        "选中图片数目：" + SelectImages.size());
                //此函数有问题
                if (e.isPopupTrigger()){
                    PopupMenu.OutPopupMenu(e,ImagesFile,ImagesLabel,ImagesField,ImagesPanel, SelectImages,PicturePanel,filepath);
                }
            }
            public void mousePressed(MouseEvent e) {

                SelectImages.clear();
                for (int b = 0; b < ImagesPanel.size(); b++) {
                    ImagesPanel.get(b).setBackground(Color.white);
                    ImagesField.get(b).setBackground(Color.white);
                }
                NumShow();
                //SelectImages.clear();
                pBegin = e.getPoint();
                PicturePanel.requestFocus();

                pressPoint = new Point(e.getLocationOnScreen());
                //验证点坐标函数，用于测试
                int b;
                for (b = 0; b < ImagesPanel.size(); b++) {
                    points.add(ImagesPanel.get(b).getLocationOnScreen());
                    //System.out.println("图片"+ b +" " + ImagesPanel.get(b).getLocationOnScreen());
                }
            }
        });
    }



    //打印左下角的显示文件夹图片信息
    public void NumShow(){
        double Storage = 0;
        for (int i = 0;i < ImagesPanel.size();i++){
            Storage += ImagesFile.get(i).length();
        }
        ImageNum.setText("共" + ImagesPanel.size() + "张("+String.format("%.2f",Storage/1024/1024)+"MB)"+"-" +
                "选中图片数目：" + SelectImages.size());
    }
}

