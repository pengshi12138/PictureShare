package ComponentTool;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.plaf.basic.BasicSliderUI;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

public class DemoSliderBarUI extends BasicSliderUI {

    Color f1 = new Color(242,242,242);
    Color f2 = new Color(170,170,170);

    Color f4 = new Color(161,161,161);
    Color f3 = new Color(100,100,100);

    Color now1,now2;

    /**
     * Constructs a {@code BasicSliderUI}.
     *
     * @param b a slider
     */
    public DemoSliderBarUI(JSlider b) {
        super(b);
        now1 = f3;
        now2 = f4;
        b.setBorder(null);
//        b.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseEntered(MouseEvent e) {
//                super.mouseEntered(e);
//                now1 = f1;
//                now2 = f2;
//            }
//            @Override
//            public void mouseExited(MouseEvent e) {
//                super.mouseEntered(e);
//                now1 = f3;
//                now2 = f4;
//            }
//        });
    }

    @Override
    public Dimension getPreferredSize(JComponent c) {
        // TODO Auto-generated method stub
        c.setPreferredSize(new Dimension(220, 40));
        return super.getPreferredSize(c);
    }

    @Override
    public void paintThumb(Graphics g){
        Graphics2D g2d=(Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        //填充椭圆框为当前thumb位置
        //g2d.fillOval(thumbRect.x,thumbRect.y ,thumbRect.width, thumbRect.height);
        Image i = null;
        try {
            i = ImageIO.read(new File("src/Icon/SliderIcon/白竖椭圆.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //也可以帖图(利用鼠标事件转换image即可体现不同状态)
        g2d.drawImage(i,thumbRect.x,thumbRect.y,thumbRect.width ,thumbRect.height ,null);
    }
    @Override
    public void paintTrack(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        int cy,cw;
        Rectangle trackBounds = trackRect;

        cy = trackBounds.height/2 - 2;
        cw = trackBounds.width;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
//        System.out.println(trackBounds.x+"  " +trackBounds.y);
        g2.translate(trackBounds.x, trackBounds.y + cy );

        //填充Track
        g2.setPaint(now2);
        //System.out.println(cw+"  " +cy);
        g2.fillRect(0,0,cw,4);


        int trackLeft = 0;
        int trackRight = 0;

        trackRight = trackRect.width - 1;

        int middleOfThumb = 0;

        int fillLeft = 0;

        int fillRight = 0;
        //坐标换算
        middleOfThumb = thumbRect.x + (thumbRect.width / 2);
        middleOfThumb -= trackRect.x;
        if (!drawInverted()) {
            fillLeft = !slider.isEnabled() ? trackLeft : trackLeft + 1;
            fillRight = middleOfThumb;
        } else {
            fillLeft = middleOfThumb;
            fillRight = !slider.isEnabled() ? trackRight - 1
                    : trackRight - 2;
        }

        g2.setPaint(now1);
        g2.fillRect(0, 0, fillRight - fillLeft, 4);

        //g2.setPaint(slider.getBackground());
//        Polygon polygon = new Polygon();
//        polygon.addPoint(0, cy);
//        polygon.addPoint(0, -cy);
//        polygon.addPoint(cw, -cy);
//        g2.fillPolygon(polygon);
//        polygon.reset();

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
        g2.translate(-trackBounds.x, -(trackBounds.y + cy));
    }

    public static void main(String[] args) {
        JFrame jf = new JFrame();
        JSlider jSlider = new JSlider();

        jSlider.setUI(new DemoSliderBarUI(jSlider));
        jSlider.setPreferredSize(new Dimension(220,40));
        jf.setSize(new Dimension(400,400));
        JPanel jPanel = new JPanel();
        jPanel.setPreferredSize(new Dimension(220,40));
        jPanel.add(jSlider);
        jf.add(jPanel);
        jf.setVisible(true);
    }
}
