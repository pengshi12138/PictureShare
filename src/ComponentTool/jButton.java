package ComponentTool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

//设置按钮样式，方便一起设置，使用继承类
public class jButton extends JButton {
    public jButton(String s) {
        super(s);
        setBorderPainted(false);
        setFont(new java.awt.Font("微软雅黑", Font.BOLD, 15));
        setBackground(Color.white);
        setFocusPainted(false);
        setMargin(null);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(new Color(218, 218, 218));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(Color.white);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                setBackground(new Color(218, 218, 218));
            }
        });
    }

    public jButton() {
        super();
        setBorderPainted(false);
        setFont(new Font("微软雅黑", Font.BOLD, 15));
        setBackground(Color.white);
        setFocusPainted(false);
        setMargin(null);
    }

    public jButton(ImageIcon i) {
        super(i);
        setBorderPainted(false);
//        setContentAreaFilled(false);
        setFont(new java.awt.Font("微软雅黑", Font.BOLD, 15));
        setBackground(new Color(34,34,34));
        setFocusPainted(false);
        setMargin(null);
    }
}
