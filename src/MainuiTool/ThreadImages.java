package MainuiTool;
/**
 * Simple To Introduction
 * 多线程处理图片
 * @Author:pengshi
 * @Version:[v1.0]
 * 使用多线程处理图片加快运行速度
 *    */


import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class ThreadImages implements Runnable {

    //桌面右侧显示面板三大组件
    ArrayList<JLabel> SmallLabels;
    ArrayList<JTextField> SmallTextFields;
    ArrayList<JPanel> SmallPanels;
    JPanel ImagePanel;
    int max;
    ArrayList<File> ImageFiles;
    int k;

	 public ThreadImages(ArrayList<File> ImageFiles, int k, int max, ArrayList<JLabel> SmallLabels,
                         ArrayList<JTextField> SmallTextFields, ArrayList<JPanel> SmallPanels, JPanel ImagePanel) {
        this.k = k;
        this.ImageFiles = ImageFiles;
        this.max = max;
        this.SmallLabels = SmallLabels;
        this.SmallTextFields = SmallTextFields;
        this.SmallPanels = SmallPanels;
        this.ImagePanel = ImagePanel;
    }

    public synchronized void ChangeImage(ImageIcon i,int w,int h,int j){
        Image im = i.getImage().getScaledInstance(w, h, Image.SCALE_DEFAULT);//改变大小
        ImageIcon ic2 = new ImageIcon(im);//从新得到一个固定图片
        try {
            SmallLabels.get(j).setIcon(ic2);//定义此组件将显示的图标
        } catch (IndexOutOfBoundsException e){
            System.out.println(j);
        }
        ic2.setImageObserver(SmallLabels.get(j));//设置图像的图像观察者,ImageObserver是异步的事实是非常重要的
        SmallTextFields.get(j).setText(ImageFiles.get(j).getName());//设置名字
    }

    public synchronized void run() {
        //System.out.println();
        for (int j = k; j < max ; j+=6) {
            //System.out.println(j);
            ImageIcon ic1 = new ImageIcon(ImageFiles.get(j).getAbsolutePath());//从路径中构造ImageIcon类
            int h1 = ic1.getIconHeight();
            int w1 = ic1.getIconWidth();//图像长宽
            if (h1 < 77 && w1 < 100) {
                ChangeImage(ic1 ,w1 ,h1 ,j);
            } else {
                if (h1 * 180 > w1 * 142) {
                    ChangeImage(ic1 ,(int)(w1 / ((double)h1 / 81)) ,81 ,j);
                } else {
                    ChangeImage(ic1 ,105 ,(int)(h1 / ((double)w1 / 105)) ,j);
                }
            }

            SmallPanels.get(j).setBounds(j % 5 * 150 + 15, 5 + (j / 5 * 140), 120, 130);//设置边界
            ImagePanel.add(SmallPanels.get(j));
            SmallPanels.get(j).setLayout(new BorderLayout(0, 0));
            SmallPanels.get(j).add(SmallLabels.get(j), BorderLayout.CENTER);
            SmallPanels.get(j).add(SmallTextFields.get(j), BorderLayout.PAGE_END);

            SmallTextFields.get(j).setBorder(null);//设置此组件的边框
            SmallTextFields.get(j).setHorizontalAlignment(SwingConstants.CENTER);//设置标签内容沿X轴的对齐方式。
            SmallTextFields.get(j).setEditable(false);//设置此组件可编辑

            //激活requestFor方法，使标签可以显示，很重要，没有的话，图片不能显示
            if (j == 0) {
                SmallLabels.get(0).setDisplayedMnemonic(501);//指定表示助记键的键码
            } else {
                SmallLabels.get(j).setDisplayedMnemonic(j);
            }
            SmallLabels.get(j).setHorizontalAlignment(SwingConstants.CENTER);//设置标签图片的对齐方式。
            //SmallLabels.get(j).setOpaque(false);//组件绘制其边界内的每个像素
        }
    }
}
