package MainuiTool;
/**
 * 图像处理类
 */
/**
 *
 * @Version: [v1.0]
 */

import javax.swing.*;
import java.awt.*;

public class newImageIcon {
    public static ImageIcon GetImageIcon(ImageIcon imageicon) {// 改变图片大小
        double h1 = imageicon.getIconHeight();
        double w1 = imageicon.getIconWidth();
        if (h1 < 77 && w1 < 100) {
            Image image = imageicon.getImage().getScaledInstance((int) w1, (int) h1, Image.SCALE_DEFAULT);// 改变大小
            return new ImageIcon(image);

        } else {
            Image image;// 改变大小
            if (h1 * 180 > w1 * 142) {
                image = imageicon.getImage().getScaledInstance((int) (w1 / (h1 / 81)), 81, Image.SCALE_DEFAULT);
            } else {
                image = imageicon.getImage().getScaledInstance(105, (int) (h1 / (w1 / 105)), Image.SCALE_DEFAULT);
            }
            return new ImageIcon(image);
        }
    }
}
