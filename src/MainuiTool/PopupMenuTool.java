package MainuiTool;

import UI.MainUI;
import test.SafeBoxUI;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class PopupMenuTool extends JPopupMenu {
    public JPanel PicturePanel; //图片显示面板
    public ArrayList<JPanel> ImagesPanel; //预览框内的图片介绍面板
    public ArrayList<JLabel> ImagesLabel; //预览框下的缩略图片
    public ArrayList<File> ImagesFile ;//预览图片加载
    public ArrayList<Integer> SelectImages;
    public ArrayList<JTextField> ImagesField ;//预览框下的文字
    public ArrayList<File> CopyFile = new ArrayList<>();//复制或者粘贴的元素
    //public JFrame MainUI;
    String filepath;
    JMenuItem Copy = new JMenuItem(" 复制 "); // 菜单中的复制选项
    JMenuItem Refresh = new JMenuItem(" 刷新 "); // 菜单中的刷新选项
    JMenuItem Paste = new JMenuItem(" 粘贴 "); //菜单中的粘贴选项
    JMenuItem Delete = new JMenuItem(" 删除 "); // 菜单中的删除选项
    JMenuItem Cut = new JMenuItem(" 剪切 "); // 菜单中的剪切选项
    JMenuItem Rename = new JMenuItem(" 重命名 "); // 菜单中的重命名选项
    JMenuItem ImageMessage = new JMenuItem(" 属性 "); // 菜单中的图片属性选项
    JMenuItem Encryption = new JMenuItem(" 加密 "); // 菜单中的图片属性选项

    JDialog RNPanel = new JDialog();//重命名窗口弹出操作
    JLabel Name = new JLabel("名    称:");//重名名名字
    JTextField Name_text = new JTextField();//文本窗口
    JLabel Number = new JLabel("起始编号:");//重命名窗口起始编号
    JTextField Number_text = new JTextField();//文本窗口
    JLabel Digits = new JLabel("编号位数:");//重命名窗口编号尾数
    JTextField Digits_text = new JTextField();//文本窗口
    JButton Y = new JButton("确定");//确定按钮
    JButton N = new JButton("取消");//取消按钮
    String RName;
    int RNumber;
    int RDigits;

    public static Boolean HaveFlag = false;//判断文件名字是否存在
    public static Boolean Flag = false;//是否改名字
    public String OldName; //重命名之前的名字
    ImageIcon TemporaryIcon;//临时图片
    File TemporaryFile;//临时文件



    public PopupMenuTool(){
        add(Refresh);
        addSeparator();       // 添加一条分隔符
        add(Copy);// 弹出式窗口中加入复制菜单项
        addSeparator();       // 添加一条分隔符
        add(Paste);// 弹出式窗口中加入粘贴菜单项
        addSeparator();       // 添加一条分隔符
//        add(Cut); // 弹出式窗口中加入剪切菜单项
//        addSeparator();       // 添加一条分隔符
        add(Delete); // 弹出式窗口中加入删除菜单项
        addSeparator(); // 往菜单中加横线
        add(Rename); // 弹出式窗口中加入重命名菜单项
        addSeparator(); // 往菜单中加横线
        add(ImageMessage); // 弹出式窗口中加入属性菜单项
        addSeparator();       // 添加一条分隔符
        add(Encryption);// 弹出式窗口中加入加密菜单项
        Listener();
    }
    /** 弹出窗口 */
    public void OutPopupMenu(MouseEvent e,ArrayList<File> ImagesFile, ArrayList<JLabel> ImagesLabel,
                             ArrayList<JTextField> ImagesField, ArrayList<JPanel> ImagesPanel,
                             ArrayList<Integer> SelectImages, JPanel PicturePanel,String filepath) {
        this.ImagesField = ImagesField;
        this.ImagesFile = ImagesFile;
        this.ImagesLabel = ImagesLabel;
        this.ImagesPanel = ImagesPanel;
        this.PicturePanel = PicturePanel;
        this.SelectImages = SelectImages;
        this.filepath = filepath;
        ImageMessage.setEnabled(true);
        Copy.setEnabled(true);
        Paste.setEnabled(true);
        Cut.setEnabled(true);
        Delete.setEnabled(true);
        Rename.setEnabled(true);
        Encryption.setEnabled(true);
        if(SelectImages.size() > 1 ){
            ImageMessage.setEnabled(false);
        }
        if(SelectImages.size() == 0){
            ImageMessage.setEnabled(false);
            Copy.setEnabled(false);
            Cut.setEnabled(false);
            Rename.setEnabled(false);
            Delete.setEnabled(false);
            ImageMessage.setEnabled(false);
            Encryption.setEnabled(false);
        }
        //按钮监听事件函数
        this.show(e.getComponent(), e.getX(), e.getY());

        //弹出窗口来进行重命名操作
        RNPanel.setSize(400,200);
        RNPanel.setTitle("重命名");
        RNPanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,10)); //流式布局
        RNPanel.setLocationRelativeTo(null);
        RNPanel.setResizable(false); //设置不要缩放

        Dimension dim1 = new Dimension(300,30);//设置文本框长度
        Dimension dim2 = new Dimension(100,30);//设置按钮长度
        Font font = new Font("宋体",Font.PLAIN,14);

        Name.setFont(font);
        RNPanel.add(Name);
        Name_text.setPreferredSize(dim1);
        RNPanel.add(Name_text);
        Number.setFont(font);
        RNPanel.add(Number);
        Number_text.setPreferredSize(dim1);
        RNPanel.add(Number_text);
        Digits.setFont(font);
        RNPanel.add(Digits);
        Digits_text.setPreferredSize(dim1);
        RNPanel.add(Digits_text);
        Y.setPreferredSize(dim2);
        RNPanel.add(Y);
        N.setPreferredSize(dim2);
        RNPanel.add(N);

        RNListener();

    }

    /** 重命名监听事件函数 */
    public void RNListener(){
        Y.addActionListener(e -> {
            RName = Name_text.getText();
            try{
                RNumber = Integer.parseInt(Number_text.getText());
                RDigits = Integer.parseInt(Digits_text.getText());
            }catch (Exception event){
                System.out.println("无法获取数值");
            }

            int i , a;
            for (a = 0 ; a < SelectImages.size() ; a++){
                String d = "";
                for (i = 0 ; i < RDigits;i ++){
                    d += "0";
                }
                //System.out.println(RName + " " + RNumber + " " +RDigits);
                String str1 = RName + new DecimalFormat(d).format(RNumber);
                int index = ImagesField.get(SelectImages.get(a)).getText().lastIndexOf(".");
                str1 = str1 + ImagesField.get(SelectImages.get(a)).getText().substring(index);
                //System.out.println(SelectImages.get(a)+" "+str1 +" "+ImagesField.get(SelectImages.get(a)).getText());//判断定位
                try {
                    RenameText(SelectImages.get(a),str1);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                RNumber ++;
            }
            Name_text.setText("");
            Number_text.setText("");
            Digits_text.setText("");
            RNPanel.dispose();
        });
        N.addActionListener(e -> {
            Name_text.setText("");
            Number_text.setText("");
            Digits_text.setText("");
            RNPanel.dispose();
        });
    }

    public void Listener(){
        ImageMessage.addActionListener(evt -> {
                File picture = ImagesFile.get(SelectImages.get(0));
                ImageIcon img = new ImageIcon(ImagesFile.get(SelectImages.get(0)).getAbsolutePath());
                double imageSize = picture.length()/1024.0;
                double h = img.getIconHeight();
                double w = img.getIconWidth();
                JFrame IntroduceFrame = new JFrame();
                JTextArea IntroduceTextArea = new JTextArea();
                IntroduceFrame.setVisible(true);
                IntroduceFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);// 点叉的时候只是隐藏面板
                IntroduceFrame.setSize(220, 130);
                IntroduceFrame.setLocationRelativeTo(null); // 弹出面板时在屏幕的中央
                IntroduceFrame.setResizable(false);//设置不要缩放
                IntroduceFrame.add(IntroduceTextArea);
                IntroduceTextArea.setEditable(false); // 文本设为不可编辑
                IntroduceTextArea.setText("\n      图片大小:"+(int)imageSize+"KB\n" + "      宽像素:"+(int)w+"\n" + "      高像素:"+(int)h+"\n");
            });
        //复制操作
        Copy.addActionListener(e -> CopyAndCut());

        //有点问题
        Cut.addActionListener(e -> {
            CopyAndCut();
            DeleteFile();
        });

        Paste.addActionListener(e -> {
            if(!CopyFile.isEmpty()) {
                //System.out.println("NiHao");
                String str ;
                int i;
                for (i = 0; i < CopyFile.size(); i++) {
                    str = CopyFile.get(i).getName();
                    //System.out.println(str);
                    int num = 1;
                    String RealName;
                    int a = str.indexOf("(");
                    int b = str.indexOf(")");
                    if (a > 0 && b > 0 && a < b){
                        RealName = str.substring(0,a);
                        //System.out.println(RealName);
                    }
                    else {
                        int index = str.indexOf(".");
                        RealName = str.substring(0,index);
                    }
                    while (repetition(str)) {
                        int index = str.indexOf(".");
                        str = RealName + "(" + num +")" + str.substring(index);
                        num++;
                    }

                    //开始文件io复制操作
                    try {
                        FileInputStream fi = new FileInputStream(CopyFile.get(i));
                        FileOutputStream fo = new FileOutputStream(filepath + File.separator + str);
                        //System.out.println("文件大小=" + fi.available());

                        byte[] f = new byte[fi.available()];
                        fi.read(f);
                        fo.write(f);
                    } catch (Exception fileNotFoundException) {
                        System.out.println("1");
                    }
                }
                //OperationFlag = 1;
                MainUI.Main.Show(new File(filepath));
                //System.out.println(str);
            }
            CopyFile.clear();
        });
        Delete.addActionListener(e -> {
            if (JOptionPane.showConfirmDialog(null,
                    "你确定要删除这" + SelectImages.size() + "张吗?","删除",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                DeleteFile();//删除函数
                MainUI.Main.Show(new File(filepath));//重新刷新界面
            }
        });


        //刷新操作
        Refresh.addActionListener(e -> MainUI.Main.Show(new File(filepath)));

        //重命名操作
        Rename.addActionListener(e -> {
            if (SelectImages.size() > 0 ){
                if (SelectImages.size() == 1){
                    //System.out.println(SelectImages.get(0));
                    RN(SelectImages.get(0));
                    Flag =false;
                }
                else {
                    RNPanel.setVisible(true);
                }
            }
        });


        /**加密操作**/
        Encryption.addActionListener(e -> {
            JFrame jFrame = new JFrame();
            jFrame.setLayout(new BorderLayout());
            jFrame.setSize(400,100);
            jFrame.setTitle("输入密码");
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
            jFrame.setResizable(false);
            JPanel jpanel = new JPanel();
            final JPasswordField textField = new JPasswordField(10);
            textField.setFont(new Font(null, Font.PLAIN, 20));
            jpanel.add(textField);
            JButton btn = new JButton("提交");
            btn.setFont(new Font(null, Font.PLAIN, 20));
            btn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (new String(textField.getPassword()).equals("8888")){
                        jFrame.setVisible(false);

                        SafeBoxUI.isFolder();//保险柜文件夹存在与否
                        for (int i = 0;i < SelectImages.size();i++){

                            File f = ImagesFile.get(SelectImages.get(i));
                            String fName = f.getName();
//                            System.out.println(fName);
                            //进行加密操作
                            try {
                                //调用主界面的保险柜加密
                                MainUI.Main.safeBoxUI.encryption(f);
                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                            }
                            f.delete();
                        }
                        JOptionPane.showMessageDialog(null, "请耐心等待", "正在加密",
                                JOptionPane.INFORMATION_MESSAGE);
                        MainUI.Main.Show(new File(filepath));//重新刷新界面
                    }
                    //System.out.println("密码: " + textField.getText());
//                    File_Encrypt_And_Decrypt a = new File_Encrypt_And_Decrypt(filepath,textField.getText(),
//                            textField.getText().length(),"C:\\Users\\22806\\OneDrive\\图片\\保险柜",1);
                }
            });
            jpanel.add(btn);
            jFrame.setContentPane(jpanel);
            jFrame.setVisible(true);
        });
    }

    /** 重命名操作 */
    public void RN(int i){

        ImagesField.get(i).setEditable(true);
        ImagesField.get(i).setBackground(Color.white);
        ImagesField.get(i).setBorder(BorderFactory.createLineBorder(Color.black, 1));
        ImagesField.get(i).grabFocus();
        ImagesField.get(i).setSelectionStart(0);
        int index = ImagesField.get(i).getText().indexOf(".");
        OldName = ImagesField.get(i).getText();//设置旧的名字，以便新名字与其他名字发生冲突
        ImagesField.get(i).setSelectionEnd(index);



//        if (!TemporaryFile.renameTo(TemporaryFile)) {// 重命名此抽象路径名表示的文件
//            ImagesLabel.get(SelectImages.get(i)).setIcon(null);// 检查返回值以确保重命名操作成功
//        }


        //ImagesField.get(SelectImages.get(i)).setFocusable(true);
        //有很大的问题

        //ImagesField.get(SelectImages.get(i)).setEditable(false);

        /** 重命名操作光标移除的操作 */
        ImagesField.get(i).addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                String str = ImagesField.get(i).getText();
                //System.out.println(str);
                if (!Flag){
                    try {
                        RenameText(i,str);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    ImagesField.get(i).setBackground(Color.WHITE);
                    Flag = !Flag;
                }

            }
        });

//实时监听事件
//        ImagesField.get(i).getDocument().addDocumentListener(new DocumentListener() {
//
//            String str;
//            protected void changeFilter(DocumentEvent event) {
//
//                javax.swing.text.Document document = event.getDocument();
//                try {
//                    str = filepath + File.separator + document.getText(0, document.getLength());
//                    //ImagesField.get(SelectImages.get(i)).setText(str);
//                    //System.out.println(ImagesField.get(i).getText());
//
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                    System.err.println(ex);
//                }
//            }
//
//            @Override
//            public void insertUpdate(DocumentEvent e) {
//                changeFilter(e);
//            }
//
//            @Override
//            public void removeUpdate(DocumentEvent e) {
//                changeFilter(e);
//            }
//
//            @Override
//            public void changedUpdate(DocumentEvent e) {
//                System.out.println("1");
//                changeFilter(e);
//            }
//        });

        ImagesField.get(i).addActionListener((e -> {

            //ImagesField.get(i).setEditable(false);
            String str = ImagesField.get(i).getText();
//            system.out.println(str);
            //System.out.println(e.getActionCommand());
            if (!Flag){
                try {
                    RenameText(i,str);
                    //ImagesField.get(i).setBackground(Color.WHITE);
                } catch (IOException ioException) {
                    System.out.println("3");
                }
                Flag = !Flag;
            }

        }));

    }
    /** 重命名文本 **/
    public void RenameText(int i,String str) throws IOException {
        TemporaryFile = ImagesFile.get(i);//临时文件
        TemporaryIcon = (ImageIcon) ImagesLabel.get(i).getIcon();// 创建临时图像
        HaveFlag = false;
        for (int a = 0;a < ImagesField.size() ; a++){
            //System.out.println();
            if (a != i && str.equals(ImagesField.get(a).getText())){
                JOptionPane.showMessageDialog(null, "文件名已经存在",
                        "重命名", JOptionPane.INFORMATION_MESSAGE);
                HaveFlag = true;
            }
        }
        if (!HaveFlag){
            String str1 = filepath + File.separator +  str;
            //System.out.println(ImagesField.get(SelectImages.get(i)).getText());
            File fileTwo = new File( str1 );// 读入新名字

            if (TemporaryFile.renameTo(fileTwo)) {// 重命名
                ImagesField.get(i).setText(str);
                //System.out.println("重命名操作成功");
            } else {
                //System.out.println("文件被其他程序占用，重命名操作失败");
                JOptionPane.showMessageDialog(null, "文件被其他程序占用，重命名操作失败",
                        "重命名", JOptionPane.INFORMATION_MESSAGE);

            }
            ImagesField.get(i).setBorder(null);
            ImagesField.get(i).setBackground(new Color(204, 232, 255));
            ImagesField.get(i).setEditable(false);
            ImagesLabel.get(i).setIcon(newImageIcon.GetImageIcon(new ImageIcon(fileTwo.getAbsolutePath())));// 显示新名字
            ImagesLabel.get(i).setBackground(Color.WHITE);
        }
        else{
            //System.out.println(ImagesField.get(i).getText());
            ImagesField.get(i).setText(OldName);
            //System.out.println(ImagesField.get(i).getText());
            ImagesField.get(i).setEditable(false);
            ImagesField.get(i).setBorder(null);
            ImagesField.get(i).setBackground(Color.WHITE);
//            ImagesLabel.get(i).setIcon(TemporaryIcon);
//            ImagesLabel.get(i).setBackground(Color.WHITE);
        }
    }
    /** 复制文件函数 */
    public void CopyAndCut(){
        CopyFile.clear();
        if(SelectImages.size() > 0){
            //System.out.println(SelectImages.size());
            int i;
            for ( i = 0 ; i < SelectImages.size() ; i++){
                CopyFile.add(ImagesFile.get(SelectImages.get(i)));
                //System.out.println(CopyFile.get(SelectImages.get(i)).getAbsolutePath());
            }
        }
        //System.out.println(filepath);
    }

    /** 删除文件函数 */
    public void DeleteFile(){
        if(SelectImages.size() > 0){
            //System.out.println(SelectImages.size());
            int i;
            for (i = 0 ; i < SelectImages.size() ; i++){
                ImagesFile.get(SelectImages.get(i)).delete();
            }
        }
    }

    /** 判断文件名字是否重复 */
    public Boolean repetition(String str){
        int a;
        for (a = 0;a < ImagesFile.size();a++){
            if(ImagesFile.get(a).getName().equals(str)){
                return true;
            }
        }
        return false;
    }

}
