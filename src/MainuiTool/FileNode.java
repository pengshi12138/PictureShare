package MainuiTool;
/**
 * 目录树节点处理类
 */

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.*;
import java.util.Objects;

public class FileNode extends DefaultMutableTreeNode {
    public boolean explored_ = false; //此数据表示是否该文件夹被打开过
    public FileNode(File file){
        setUserObject(file);//将此节点的用户对象设置为 file，就是数据转换，符合TreeNode的节点
    }
    public boolean isLeaf()   {
        return !isDirectory();//返回是否属于叶子节点，判断是不是文件夹
    }

    public File getFile()   {
        return (File)getUserObject();//返回此节点的用户对象，强制转换为文件object
    }
    public boolean isExplored()   {
        return explored_;//返回是否遍历过
    }
    public boolean isDirectory()   {
        File file = getFile();
        return file.isDirectory();//返回是否为文件夹
    }
    public String toString()   {//重写toString方法，返回文件名
        File file = getFile   ();
        String filename = file.toString();//获得当前节点文件名
        int index = filename.lastIndexOf("\\");//返回指定子字符串最后一次出现的字符串中的索引
        return (index != -1 && index != filename.length() - 1) ?
                filename.substring(index + 1) : filename;
    }
    public String getString()   {
        File file = getFile();
        return file.getAbsolutePath();
    }
    public File getWorR(){
        File file=getFile();
        return file.getAbsoluteFile();
    }
//        public String getWorR1() throws IOException{
//        File file=getFile();
//        String file1=file.getPath();//将此抽象路径名转换为路径名字符串
//        return file1;
//    }
    public void explore(){
        if (!isDirectory()){//是文件夹则返回
            return;
        }
        if (!isExplored() && !explored_)   {//没有被遍历,加上一个标识符explored
            File file = getFile   ();
            File[] children = file.listFiles();//遍历子文件
            int i;
            for(i = 0; i < Objects.requireNonNull(children).length; ++i){
                if(children[i].isDirectory()){
                    add(new FileNode(children[i]));//将文件夹加入
                }
            }
            explored_ = true;
        }
    }
}
