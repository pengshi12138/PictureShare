package ImageTool;
import org.opencv.core.*;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class OpencvI {
    static {
        //在使用OpenCV前必须加载Core.NATIVE_LIBRARY_NAME类,否则会报错
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {
        Image i = Sketch("D:/1.jpg");

        JFrame jf = new JFrame();
        jf.setSize(new Dimension(1500,900));
        jf.setTitle("00");
        jf.setLayout(new BorderLayout());
        jf.setMinimumSize(new Dimension(1000,650));
        jf.getContentPane().setBackground(Color.WHITE);
        JLabel a = new JLabel();
        a.setIcon(new ImageIcon(i));
        jf.add(a);
        jf.setVisible(true);
    }
    /** 格式转换 Mat到BufferedImage */
    public static BufferedImage Mat2BufImg (Mat matrix, String fileExtension) {

        MatOfByte mob = new MatOfByte();
        Imgcodecs.imencode(fileExtension, matrix, mob);
        // convert the "matrix of bytes" into a byte array
        byte[] byteArray = mob.toArray();
        BufferedImage bufImage = null;
        try {
            InputStream in = new ByteArrayInputStream(byteArray);
            bufImage = ImageIO.read(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bufImage;
    }
    /** 获取图片后缀名 */
    public static String getFileExtension(String filepath){
        filepath = new String(filepath.getBytes(StandardCharsets.UTF_8));
        int index = filepath.lastIndexOf(".");
        return filepath.substring(index);
    }
    /**
     * OpenCV-4.0.0 获取图像像素点
     *
     * @return: void
     * @date: 2020.5.1
     */
    /** 素描滤镜 */
    public static Image Sketch(String filepath) {

        Mat src = readFile(new File(filepath));
        Mat gray = new Mat(src.size(),CvType.CV_8UC3);

        double[] pixel = new double[3];//用于存放像素点
        double[] pixel1 = new double[3];//用于存放像素点
        //1、去色
        for (int i = 0, RLen = src.rows(); i < RLen; i++) {
            for (int j = 0, CLen = src.cols(); j < CLen; j++) {
                pixel = src.get(i, j).clone();
                double max = 0,min = 256 ;
                for (int a = 0 ; a < 3 ; a ++){
                    if (max < pixel[a]) max = pixel[a];
                    if (min > pixel[a]) min = pixel[a];
                }
                double pin = ( max + min ) / 2 ;
                pixel1[0] = pin;//B
                pixel1[1] = pin;//G
                pixel1[2] = pin;//R
                gray.put(i, j, pixel1);
            }
        }

        //2、复制去色图层，并且反色
        Mat gray_reversal = new Mat(gray.size(),CvType.CV_8UC3);
        for (int i = 0, RLen = gray.rows(); i < RLen; i++) {
            for (int j = 0, CLen = gray.cols(); j < CLen; j++) {
                pixel = gray.get(i, j).clone();
                pixel1[0] = 255 - pixel[0];//B
                pixel1[1] = 255 - pixel[1];//B
                pixel1[2] = 255 - pixel[2];//R
                gray_reversal.put(i, j, pixel1);
            }
        }

        //3、对反色图像进行高斯模糊；
        Imgproc.GaussianBlur(gray_reversal,gray_reversal, new Size(7,7),0);
        //4、模糊后的图像叠加模式选择颜色减淡效果。
        // 减淡公式：C =MIN( A +（A×B）/（255-B）,255)，其中C为混合结果，A为去色后的像素点，B为高斯模糊后的像素点。
        Mat result =  new Mat(gray.size(), CvType.CV_8UC3);
        for (int i = 0, rlen = src.rows(); i < rlen; i++) {
            for (int j = 0, clen = src.cols(); j < clen; j++) {
                for (int n = 0 ; n < 3 ; n ++){
                    double a = gray.get(i,j)[n];
                    double b = gray_reversal.get(i,j)[n];
                    double c = Math.min(a + (a * b) / (255 - b),255);
                    pixel[n] = c;
                }
                result.put(i, j, pixel);
            }
        }
//        Imgcodecs.imwrite("C:/Users/22806/Desktop/5.png",result);
        return Mat2BufImg(result,getFileExtension(filepath));
    }
    /** 连环画滤镜 */
    public static Image Comic(String filepath) {

        Mat src = readFile(new File(filepath));
        Mat dst = new Mat(src.size(), src.type());

        double[] pixel;//用于存放像素点

        for (int i = 0, rlen = src.rows(); i < rlen; i++) {
            for (int j = 0, clen = src.cols(); j < clen; j++) {
                pixel = src.get(i, j).clone();
                double b = pixel[0],g = pixel[1],r = pixel[2];
                pixel[0] = Math.abs(b - g + b + r) * g / 256;//B
                pixel[1] = Math.abs(b - g + b + r) * r / 256;//G
                pixel[2] = Math.abs(g - b + g + r) * r / 256;//R
                dst.put(i, j, pixel);
            }
        }

        return Mat2BufImg(dst,getFileExtension(filepath));
    }
    /** 阴间滤镜 */
    public static Image Underworld(String filepath) {

        //调用的图片的路径，图片的读取
        Mat src = readFile(new File(filepath));
        //两个参数分别是图像的大小和类型
        Mat dst = new Mat(src.size(), src.type());
        //获取图像通道数
        int channels = src.channels();
        //用于存放像素点，三通道
        double[] pixel ;

        for (int i = 0, rlen = src.rows(); i < rlen; i++) {
            for (int j = 0, clen = src.cols(); j < clen; j++) {
                if (channels == 3) {//图片为3通道即平常的(B,G,R)
                    //克隆
                    pixel = src.get(i, j).clone();
                    pixel[0] = 255 - pixel[0];//B
                    pixel[1] = 255 - pixel[1];//G
                    pixel[2] = 155 - pixel[2];//R
                    dst.put(i, j, pixel);
                } else {//图片为单通道
                    //i为图片的行，j为图片的列，第三个将图片转化的数组克隆一下
                    dst.put(i, j, src.get(i, j).clone());
                }
            }
        }

        return Mat2BufImg(dst,getFileExtension(filepath));
    }
    /** 浮雕滤镜
     * 浮雕的算法为，用当前像素点的前一个像素点灰度值减去后一个像素点的灰度值，所得结果加上128作为当前像素点的灰度值*/
    public static Image Carving(String filepath){

        Mat src = readFile(new File(filepath));
        //两个参数分别是图像的大小和类型
        Mat dst = new Mat(src.size(), src.type());
        //用于存放像素点，三通道
        double[] pixel = new double[3];
        for (int i = 1, rlen = src.rows(); i < rlen - 1; i++) {
            for (int j = 1, clen = src.cols(); j < clen - 1; j++) {
                pixel[0] = src.get(i, j)[0] - src.get(i - 1, j - 1)[0] + 128;//B
                pixel[1] = src.get(i, j)[1] - src.get(i - 1, j - 1)[1] + 128;//G
                pixel[2] = src.get(i, j)[2] - src.get(i - 1, j - 1)[2] + 128;//R
                dst.put(i, j, pixel);
            }
        }

        return Mat2BufImg(dst,getFileExtension(filepath));
    }
    /** 冰冻算法 */
    public static Image frozen(String filepath){

        Mat src = readFile(new File(filepath));
        //两个参数分别是图像的大小和类型
        Mat dst = new Mat(src.size(), src.type());
        //用于存放像素点，三通道
        double[] pixel = new double[3];
        for (int i = 1, RLen = src.rows(); i < RLen - 1; i++) {
            for (int j = 1, CLen = src.cols(); j < CLen - 1; j++) {
                pixel = src.get(i, j).clone();
                double b = pixel[0],g = pixel[1],r = pixel[2];
                pixel[0] = Math.abs(b - r - g ) * 3/2;//B
                pixel[1] = Math.abs(g - r - b ) * 3/2;//G
                pixel[2] = Math.abs(r - g - b ) * 3/2;//R
                dst.put(i, j, pixel);
            }
        }

        return Mat2BufImg(dst,getFileExtension(filepath));
    }
    /**动漫滤镜 */
    public static Image Cartoon(String filepath){

        Mat img = readFile(new File(filepath));
        //两个参数分别是图像的大小和类型
        Mat img1 = img.clone();
        Mat img2 = new Mat(img1.size(), img1.type());
        //1.应用双边滤镜以减少图像的调色板
        int num_bilateral = 7;//双边过滤步数
        int H = img1.rows();
        int W = img1.cols();
        int H1 = H / 2;
        int H2 = H / 4;
        int W1 = W / 2;
        int W2 = W / 4;

        //下采样步数2
        Imgproc.pyrDown(img1,img1,new Size(W1,H1));
        Imgproc.pyrDown(img1,img1,new Size(W2,H2));

        for (int i = 0 ; i < num_bilateral ; i++){
            Imgproc.bilateralFilter( img1 , img2,9,9,7);
        }
        Imgproc.pyrUp(img2,img2,new Size(W1,H1));
        Imgproc.pyrUp(img2,img2,new Size(W,H));

        //2：使用中值滤波器降低噪声
        Mat gray = new Mat(img.size(),CvType.CV_8UC3);
        Mat blur = new Mat(img.size(),CvType.CV_8UC3);
        Imgproc.cvtColor(img,gray,Imgproc.COLOR_RGB2GRAY);
        Imgproc.medianBlur(gray,blur,7);

        //3.使用自适应阈值创建边缘遮罩
        Mat edge = new Mat(img2.size(),CvType.CV_8UC3);
        Imgproc.adaptiveThreshold(blur,edge,255,Imgproc.ADAPTIVE_THRESH_MEAN_C,
                Imgproc.THRESH_BINARY,9,2);

        //4.将彩色图像与边缘蒙版组合
        Mat dst = new Mat(img2.size(),CvType.CV_8UC3);
        Imgproc.cvtColor(edge,edge,Imgproc.COLOR_GRAY2RGB);
        Core.bitwise_and(img2,edge,dst);

        return Mat2BufImg(dst,getFileExtension(filepath));

    }
    /** 彩色变化 海报风格 */
    public static Image ColorChange(String filepath){

        Mat img = readFile(new File(filepath));
        int w = img.cols();
        int h = img.rows();
        Mat[] imgColor = new Mat[12];
        Mat display = new Mat(new Size(w*4,h*3), CvType.CV_8UC3);
        for(int i=0; i<12; i++)
        {
            imgColor[i] = new Mat(img.size(),img.type());
            Imgproc.applyColorMap(img,imgColor[i],i);
            int x=i%4;
            int y=i/4;
            Mat displayROI = new Mat( display, new Rect(x*w,y*h,w,h));
            Imgproc.resize(imgColor[i],displayROI,displayROI.size());
        }

        return Mat2BufImg(display,getFileExtension(filepath));
    }

    //处理中文文件报错问题

    public static Mat readFile(File filepath){
        if(isContainChinese(filepath.getAbsolutePath())){
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            byte[] byt = new byte[(int) filepath.length()];
            try {
                int read = inputStream.read(byt);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  Imgcodecs.imdecode(new MatOfByte(byt), Imgcodecs.IMREAD_COLOR);
        }
        else{
            return Imgcodecs.imread(filepath.getAbsolutePath(), Imgcodecs.IMREAD_ANYCOLOR);
        }
    }

    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }


    public static void test(String filepath){
        Mat img = Imgcodecs.imread(filepath);
        Mat i = new Mat(img.size(), img.type());
        Imgproc.bilateralFilter( img,i,9,9,7);
        HighGui.imshow("芜湖",i);
        HighGui.waitKey(0);
    }
}
