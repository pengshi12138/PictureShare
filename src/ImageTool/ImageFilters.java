package ImageTool;

import com.jhlabs.image.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;

public class ImageFilters {

    /** 创立一个和原始图片大小一致的图片容器 */
    public BufferedImage createBufferedImage(BufferedImage src) {
        ColorModel cm = src.getColorModel();
        BufferedImage image = new BufferedImage(
                cm,
                cm.createCompatibleWritableRaster(src.getWidth(), src.getHeight()),
                cm.isAlphaPremultiplied(),
                null);
        return image;
    }

    /** 旋转需要的图片大小设定 */
    public static BufferedImage createRotateBufferedImage(BufferedImage src) {
        ColorModel cm = src.getColorModel();
        BufferedImage image = new BufferedImage(
                cm,
                cm.createCompatibleWritableRaster(src.getHeight(), src.getWidth()),
                cm.isAlphaPremultiplied(),
                null);
        return image;
    }
    /** 复制图片 */
    public static BufferedImage copyBufferedImage(BufferedImage src) {
        return src.getSubimage(0,0,src.getWidth(),src.getHeight());
    }

    BufferedImage image;
    Image i;
    public ImageFilters(Image now){
        this.image = copyBufferedImage((BufferedImage)now);
        i = copyBufferedImage(image);
        Now = i;
        S = image;
    }

    public void setImage(BufferedImage image) {
       this.i = copyBufferedImage(image);
        oldContrast = 1;oldExposure = 1;oldTone = 0;oldSaturation = 0;oldBrightness = 0;oldLevels = 0;
        oldHlevels = 1;
    }

    float oldContrast = 1,oldExposure = 1,oldTone = 0,oldSaturation = 0,oldBrightness = 0,
            oldLevels = 0,oldHlevels = 1;

    float oldR = 0,oldG = 0,oldB = 0;

    Image Now ,S ;
    int changNum = 0;
    public void setFilter(int index){
        if (index == changNum) return;
        Now = i;
        changNum = index;
        if (index == 1){
            Now = ExposureFilter(Now,oldExposure);
            Now = HSBAdjustFilter(Now,oldTone,oldSaturation,0);
            Now = Brightness(Now,oldBrightness);
            Now = LevelsFilter(Now,oldLevels,oldHlevels);
            Now = RGBAdjustFilter(Now,oldR,oldG,oldB);
        }
        if (index == 2){
            Now = Contrast(Now,oldContrast);
            Now = HSBAdjustFilter(Now,oldTone,oldSaturation,0);
            Now = Brightness(Now,oldBrightness);
            Now = LevelsFilter(Now,oldLevels,oldHlevels);
            Now = RGBAdjustFilter(Now,oldR,oldG,oldB);
        }
        if (index == 3){
            Now = Contrast(Now,oldContrast);
            Now = ExposureFilter(Now,oldExposure);
            Now = Brightness(Now,oldBrightness);
            Now = LevelsFilter(Now,oldLevels,oldHlevels);
            Now = RGBAdjustFilter(Now,oldR,oldG,oldB);
        }
        if (index == 4){
            Now = Contrast(Now,oldContrast);
            Now = ExposureFilter(Now,oldExposure);
            Now = HSBAdjustFilter(Now,oldTone,oldSaturation,0);
            Now = LevelsFilter(Now,oldLevels,oldHlevels);
            Now = RGBAdjustFilter(Now,oldR,oldG,oldB);
        }
        if (index == 5){
            Now = Contrast(Now,oldContrast);
            Now = ExposureFilter(Now,oldExposure);
            Now = HSBAdjustFilter(Now,oldTone,oldSaturation,0);
            Now = Brightness(Now,oldBrightness);
            Now = RGBAdjustFilter(Now,oldR,oldG,oldB);
        }
        if (index == 6){
            Now = Contrast(Now,oldContrast);
            Now = ExposureFilter(Now,oldExposure);
            Now = HSBAdjustFilter(Now,oldTone,oldSaturation,0);
            Now = Brightness(Now,oldBrightness);
            Now = LevelsFilter(Now,oldLevels,oldHlevels);
        }
    }
    /** 调用图片过滤器函数
     * 算法介绍，通过标志符判断，进行鉴定，输出
     * 对于每个不同的图片处理保留上一次的图片处理图像，节省处理时间
     * */
    public synchronized Image filter(Image in,float contrast,float Exposure,float tone,float saturation,
                                     float brightness,float Levels,float Hlevels,
                                     float R,float G,float B){
        if (contrast != oldContrast){
            setFilter(1);
            S = Contrast(Now,contrast);
            oldContrast = contrast;
        }
        else if (Exposure != oldExposure){
            setFilter(2);
            S = ExposureFilter(Now,Exposure);
            oldExposure = Exposure;
        }
        else if (oldTone != tone || oldSaturation != saturation){
            setFilter(3);
            S = HSBAdjustFilter(Now,tone,saturation,0);
            oldTone = tone;
            oldSaturation = saturation;
        }
        else if (brightness != oldBrightness){
            setFilter(4);
            S = Brightness(Now,brightness);
            oldBrightness = brightness;
        }else if (oldLevels !=Levels || oldHlevels!= Hlevels){
            setFilter(5);
            S = LevelsFilter(Now,Levels,Hlevels);
            oldLevels = Levels;
            oldHlevels = Hlevels;
        }else if (oldR != R ||oldB != B || oldG!= G){
            setFilter(6);
            S = RGBAdjustFilter(Now,
                    R,G,B);
            oldR = R;
            oldB = B;
            oldG = G;
        }

        return S;
    }


    //测试函数
    public static void main(String[] args) {
        Image i = null;
        try {
            i = ImageIO.read(new File("D:/1.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageFilters c = new ImageFilters(i);

        MirrorFilter k = new MirrorFilter();

        //改变这个就可以
        k.setDistance(-0.5f);
        k.setRotation(90);
        k.setOpacity(0.3f);
        k.setAngle(0.1f);
        BufferedImage g = c.createBufferedImage((BufferedImage) i);
        k.filter((BufferedImage) i, g);

        JFrame jf = new JFrame();
        jf.setSize(new Dimension(800,1000));
        jf.setTitle("00");
        jf.setLayout(new BorderLayout());
        jf.setMinimumSize(new Dimension(800,1000));
        jf.getContentPane().setBackground(Color.WHITE);
        JLabel b = new JLabel();
        b.setIcon(new ImageIcon(g));
        jf.add(b);
        jf.setVisible(true);
    }
    /**亮度调节 0.5~1.5 */
    public Image Brightness(Image i,float a){
        ContrastFilter b = new ContrastFilter();
        BufferedImage r = createBufferedImage((BufferedImage) i);
        b.setBrightness(a);
        b.filter((BufferedImage)i,r);
        return r;
    }
    /** 对比度函数  0.2到0.6  */
    public Image Contrast(Image i,float a){
        ContrastFilter b = new ContrastFilter();
        BufferedImage r = createBufferedImage((BufferedImage) i);
        b.setContrast(a);
        b.filter((BufferedImage)i, r);
        return r;
    }
    /**曝光函数，a的适合值是 0.1~5 */
    public Image ExposureFilter(Image i , float a )
    {
        //曝光度函数中1的数值不能1输入
        if (a == 1) return i;
        ExposureFilter e = new ExposureFilter();
        e.setExposure(a);

        BufferedImage r = createBufferedImage((BufferedImage) i);
        e.filter((BufferedImage) i,r);
        return r;
    }
    /**颜色阈值函数，a的适合值是 0~256 b的适合值是 0~256 */
    public Image ThresholdFilter(Image i , float a , float b )
    {
        ThresholdFilter t = new ThresholdFilter();
        t.setLowerThreshold((int)a);
        t.setUpperThreshold((int)b);
        t.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    /**玻璃效果函数，a的适合值是 0~100 */
    public Image DisplaceFilter(BufferedImage i , float a )
    {
        DisplaceFilter e = new DisplaceFilter();
        e.setDisplacementMap(i);
        e.setAmount(a);
        e.filter(i,i);
        return i;
    }
    /**水下波纹函数，a的适合值是 1~10 */
    public Image SwimFilter(Image i , float a )
    {
        SwimFilter e = new SwimFilter();
        e.setAmount(a);
        e.setAngle(a);
        e.setScale(a);
        e.setStretch(a);
        e.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    /**图像伽玛函数，a的适合值是 1~10 */
    public Image GammaFilter(Image i , float a )
    {
        GammaFilter e = new GammaFilter ();
        e.setGamma(a);
        e.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    /**调整图像水平度函数，a的适合值是 1~9 */
    public Image LevelsFilter(Image i , float a ,float b)
    {
        BufferedImage r = createBufferedImage((BufferedImage) i);
        LevelsFilter e = new LevelsFilter();
        e.setLowOutputLevel(a);
        e.setHighOutputLevel(b);
        e.filter((BufferedImage) i,r);
        return r;
    }
    /**分色函数，a的适合值是 2~12 */
    public Image PosterizeFilter(Image i , float a )
    {
        PosterizeFilter e = new PosterizeFilter();
        e.setNumLevels((int)a);
        e.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    /**颜色缩放因子函数，a的适合值是 1~9 */
    public Image RescaleFilter(Image i , float a )
    {
        RescaleFilter e = new RescaleFilter();
        e.setScale(a);
        e.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    /**调节色调，饱和度和亮度函数，
     * a,b,c的适合值是 -0.3~0.3, -100~100  , c为0
     * */
    public Image HSBAdjustFilter(Image i , float a , float b , float c)
    {
        BufferedImage r = createBufferedImage((BufferedImage) i);
        HSBAdjustFilter e = new HSBAdjustFilter();
        e.setBFactor(c);
        e.setHFactor(b);
        e.setSFactor(a);
        e.filter((BufferedImage) i,r);
        return r;
    }
    /**色度滤光片函数，a,b的适合值是 0~1 0~3 */
    public Image ChromeFilter(Image i , float a , float b)
    {
        ChromeFilter e = new ChromeFilter();
        e.setAmount(a);
        e.setExposure(b);
        e.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    /**边界过滤器函数，a的适合值是 100~900 */
    public Image BorderFilter(Image i , float a )
    {
        BorderFilter e = new BorderFilter();
        e.setRightBorder((int)a);
        e.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    /**轮廓滤波器函数，a,b的适合值是 0~100 , c的适合的值 0~10 */
    public Image ContourFilter(Image i , float a , float b,float c)
    {
        ContourFilter e = new ContourFilter();
        e.setLevels(a);
        e.setOffset(b);
        e.setOffset(c);
        e.filter((BufferedImage) i,(BufferedImage)i);
        return i;
    }
    public Image RGBAdjustFilter(Image i,float r,float g,float b){
        BufferedImage k = createBufferedImage((BufferedImage) i);
        RGBAdjustFilter e = new RGBAdjustFilter(r,g,b);
        e.filter((BufferedImage) i,k);
        return k;
    }
}
