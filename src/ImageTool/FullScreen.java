package ImageTool;

import java.awt. * ;
import javax.swing. * ;

import ComponentTool.*;

public class FullScreen
{
    Image i;
    int H;
    int W;
    public FullScreen (Image i)
    {
        this.i = i ;
        GraphicsEnvironment ge  =  GraphicsEnvironment.getLocalGraphicsEnvironment ();
        GraphicsDevice gd  =  ge.getDefaultScreenDevice ();
        TestFullScreen myWindow  =   new  TestFullScreen ();

        if  (gd.isFullScreenSupported()) // 如果支持全屏
            gd.setFullScreenWindow (myWindow);
        else
            System.out.println ( " Unsupported full screen. " );
        this.H = myWindow.getHeight();
        this.W = myWindow.getWidth();
        myWindow.getContentPane().setLayout(null);
        jButton button = new jButton(new ImageIcon("src/Icon/灰退出全屏.png"));
        button.setBounds( W - 60,H - 60,60,60);
        //System.out.println((H - 60)+" "+ (W - 60));
        myWindow.getContentPane().add(button);
        button.addActionListener(e -> myWindow.dispose ());
    }
    class TestFullScreen extends JWindow
    {
        public void paint(Graphics g)
        {
            //g.drawImage(i,0,0,null);
            double w1,h1;
            int iw = i.getWidth(null);
            int ih = i.getHeight(null);
            //System.out.println(iw + " " + ih);
//            System.out.println(H+" " + W);
            if ((double) ih/iw > (double) H/W){
                w1 = (double) iw / ih * H;
                h1 = H;
            }
            else{
                w1 = W;
                h1 = (double) ih / iw * W;
            }
            g.drawImage(i,(int)(W - w1)/2,(int)(H - h1)/2,(int)w1,(int)h1,null);
        }
    }
}


