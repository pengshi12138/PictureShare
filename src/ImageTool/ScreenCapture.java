package ImageTool;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ScreenCapture extends JFrame{
    private int x1, y1, x2, y2; //鼠标位置查看
    private int recX, recY, recH, recW; // 截取的图像
    private boolean isFirstPoint = true; //查看是不是第一次点击
    private BackgroundImage labFullScreenImage = new BackgroundImage();
    private Robot robot;
    private BufferedImage fullScreenImage;
    private BufferedImage pickedImage;
    private JDialog dialog = new JDialog();

    /** ScreenCapture 构造函数*/
    public ScreenCapture(){
        getScreenCapture();//初始化操作
        setTitle("截图");
        Image i = null;
        try {
            i = ImageIO.read(new File("src/Icon/灰剪切.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setIconImage(i);
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        JLabel ImageBox = new JLabel();
        panel.add(BorderLayout.CENTER, ImageBox);
        captureImage();
        ImageBox.setIcon(getPickedIcon());
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                closing();
            }
        });

        setSize(getPickedImage().getWidth(), getPickedImage().getHeight());
        setLocationRelativeTo(null);
        setContentPane(panel);
        setVisible(true);
    }
    public void closing(){
        int n = JOptionPane.showConfirmDialog(null, "是否保存", "标题",
                JOptionPane.YES_NO_OPTION);//弹出窗口，判断是否保存
        if (n == 0) {
            try {
                JFileChooser fileChooser;
                fileChooser = new JFileChooser();// 创建文件选择对话框
                fileChooser.setSelectedFile(new File("src/Data/截图.jpg"));//临时存放截图
                FileNameExtensionFilter filter = new FileNameExtensionFilter("图像文件（JPG/GIF）",
                        "JPG", "JPEG", "GIF");// 设置文件过滤器，只列出JPG或GIF格式的图片
                fileChooser.setFileFilter(filter);
                // 打开文件选择框（线程将被阻塞, 直到选择框被关闭）
                fileChooser.showSaveDialog(this);
                ImageIO.write(getPickedImage(), "jpg", fileChooser.getSelectedFile());//写入文件
            } catch (Exception event) {
                System.out.println("");
            }
        }
        dispose();
    }
    public void getScreenCapture(){
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        JPanel jpanel = (JPanel) dialog.getContentPane();
        jpanel.setLayout(new BorderLayout());
        labFullScreenImage.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent evn) {
                isFirstPoint = true;
                pickedImage = fullScreenImage.getSubimage(recX, recY, recW,
                        recH);
                dialog.setVisible(false);
            }
        });
        labFullScreenImage.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent evn) {
                if (isFirstPoint) {
                    x1 = evn.getX();
                    y1 = evn.getY();
                    isFirstPoint = false;
                } else {
                    x2 = evn.getX();
                    y2 = evn.getY();
                    int maxX = Math.max(x1, x2);
                    int maxY = Math.max(y1, y2);
                    int minX = Math.min(x1, x2);
                    int minY = Math.min(y1, y2);
                    recX = minX;
                    recY = minY;
                    recW = maxX - minX;
                    recH = maxY - minY;
                    labFullScreenImage.drawRectangle(recX, recY, recW, recH);
                }
            }
            public void mouseMoved(MouseEvent e) {
                labFullScreenImage.drawCross(e.getX(), e.getY());
            }
        });
        jpanel.add(BorderLayout.CENTER, labFullScreenImage);
        dialog.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        dialog.setAlwaysOnTop(true);
        dialog.setMaximumSize(Toolkit.getDefaultToolkit().getScreenSize());
        dialog.setUndecorated(true);
        dialog.setSize(dialog.getMaximumSize());
        dialog.setModal(true);
    }

    /** 捕捉屏幕的一个矫形区域 */
    public void captureImage() {
        fullScreenImage = robot.createScreenCapture(new Rectangle(Toolkit
                .getDefaultToolkit().getScreenSize()));
        ImageIcon icon = new ImageIcon(fullScreenImage);
        labFullScreenImage.setIcon(icon);
        dialog.setVisible(true);
    }
    /** 得到捕捉后的BufferedImage */
    public BufferedImage getPickedImage() {
        return pickedImage;
    }
    /** 得到捕捉后的Icon */
    public ImageIcon getPickedIcon() {
        return new ImageIcon(getPickedImage());
    }
}
class BackgroundImage extends JLabel {
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.black);
        g.drawRect(x, y, w, h);
        String area = w + " * " + h;
        g.drawString(area, x +  w / 2 - 15, y +  h / 2);
        g.drawLine(lineX, 0, lineX, getHeight());
        g.drawLine(0, lineY, getWidth(), lineY);
    }
    public void drawRectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        h = height;
        w = width;
        repaint();
    }
    public void drawCross(int x, int y) {
        lineX = x;
        lineY = y;
        repaint();
    }
    int lineX, lineY;
    int x, y, h, w;
}

