package ImageTool;

import ComponentTool.jButton;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class AutoPlayWindow {
    Image i;
    int H;
    int W;
    int choose;
    FullScreen myWindow;
    ArrayList<File> ImagesFile;
    public AutoPlayWindow(Image i, ArrayList<File> ImagesFile, int choose){
        this.i = i;
        GraphicsEnvironment ge  =  GraphicsEnvironment.getLocalGraphicsEnvironment ();
        GraphicsDevice gd  =  ge.getDefaultScreenDevice ();
        myWindow  =   new FullScreen();
        if  (gd.isFullScreenSupported()) // 如果支持全屏
            gd.setFullScreenWindow (myWindow);
        else
            System.out.println ( " Unsupported full screen. " );
        this.H = myWindow.getHeight();
        this.W = myWindow.getWidth();
        this.choose = choose;
        this.ImagesFile = ImagesFile;
        myWindow.getContentPane().setLayout(null);
        //循环播放线程，设置Timer
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run(){
                runI();
            }
        },3000,3000);

        jButton button = new jButton(new ImageIcon("src/Icon/灰退出全屏.png"));
        button.setBounds( W - 60,H - 60,60,60);
        //System.out.println((H - 60)+" "+ (W - 60));
        myWindow.getContentPane().add(button);
        button.addActionListener(e -> myWindow.dispose ());
    }

    public void runI(){
        choose = ++choose % ImagesFile.size();
        try {
            i = ImageIO.read(ImagesFile.get(choose));
        } catch (IOException e) {
            e.printStackTrace();
        }

        myWindow.repaint();
    }

    class FullScreen extends JWindow
    {
        public void paint(Graphics g)
        {
            //设置背景颜色为黑色
            g.setColor(Color.black);
            g.fillRect(0,0,W,H);
            double w1,h1;
            int iw = i.getWidth(null);
            int ih = i.getHeight(null);
            if ((double) ih/iw > (double) H/W){
                w1 = (double) iw / ih * H;
                h1 = H;
            }
            else{
                w1 = W;
                h1 = (double) ih / iw * W;
            }
            g.drawImage(i,(int)(W - w1)/2,(int)(H - h1)/2,(int)w1,(int)h1,null);
        }
    }
}
