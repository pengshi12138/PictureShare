package test;

import ComponentTool.MouseEventListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;
import java.io.File;
import java.io.IOException;

//图片展示类
public class safeShowUI extends JWindow {
    public Color transparent = new Color(0,0,0,0);

    public safeShowUI(Image image){

        setLayout(null);
        setBackground(Color.WHITE);
        setSize(new Dimension(1200,900));
        setShape(new RoundRectangle2D.Double(0, 0, 1200, 900, 50, 50));


        MouseEventListener e = new MouseEventListener(this);
        addMouseListener(e);
        addMouseMotionListener(e);



        //图片展示
        double h1 = image.getHeight(null);//图像长宽
        double w1 = image.getWidth(null);
        double w,h;
        int x,y;
        if ((double) 1200/900 > w1/h1){
            h = 900;
            w = w1 * 900 / h1;
            y = 0;
            x = (int) ((1200 - w)/2);
        }else {
            w = 1200;
            h = h1 * 1200 / w1;
            x = 0;
            y = (int) ((900 - h)/2);
        }

        image = image.getScaledInstance((int)w, (int)h, Image.SCALE_DEFAULT);
        JLabel label_pic = new JLabel(new ImageIcon(image));
        label_pic.setBounds(x,y,(int)w, (int)h);
        add(label_pic);
        setLocationRelativeTo(null);

        JButton back = new JButton();
        back.setBackground(Color.WHITE);//设置按钮颜色
        back.setBorder(null);
        back.setFocusPainted(false);
        back.setIcon(new ImageIcon("src/Icon/叉.png"));
        back.setBounds(1150,0,30,30);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(back);

        setVisible(true);

    }
}

