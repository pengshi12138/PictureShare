package test;

import ComponentTool.*;
import UI.MainUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Objects;

public class SafeBoxUI extends JWindow {
    public Color transparent = new Color(0,0,0,0);
    final Shape shape = new RoundRectangle2D.Double(0d, 0d, 250,200, 50, 50);//圆角设置

    static File folder;//保险柜文件夹
    int PictureNumber;
    public ArrayList<JPanel> ImagesPanel = new ArrayList<>();//预览框内的图片介绍面板
    public ArrayList<JLabel> ImagesLabel = new ArrayList<>();//预览框下的缩略图片
    public ArrayList<File> ImagesFile = new ArrayList<>();//预览图片加载
    public ArrayList<Image> ImagesImage = new ArrayList<>();//预览图片加载
    ArrayList<Integer> SelectImages = new ArrayList<>();
    public JPanel allPanel = new JPanel(null);
    public JScrollPane allSPanel = new JScrollPane(allPanel);//滚动面板
    /** 安全库 */
    public SafeBoxUI(){
        setLayout(null);

        JPanel Tool = new JPanel(null);

        //上面工具栏配色
        Tool.setBackground(new Color(171,171,171));


        Tool.setBounds(0,0,1200,30);
        MouseEventListener e = new MouseEventListener(this);
        Tool.addMouseListener(e);
        Tool.addMouseMotionListener(e);
        add(Tool);
        setSize(new Dimension(1200,900));
        setShape(new RoundRectangle2D.Double(0, 0, 1200, 900, 50, 50));

        //内部设计
        JButton back = new JButton();
        back.setBackground(Color.white);//设置按钮颜色
        back.setBorder(null);
        back.setFocusPainted(false);
        back.setIcon(new ImageIcon("src/Icon/叉.png"));
        back.setBounds(1150,0,30,30);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        Tool.add(back);

        jButton decrypt = new jButton();
        decrypt.setBackground(Color.WHITE);
        decrypt.setBorder(null);
        decrypt.setFocusPainted(false);
        decrypt.setIcon(new ImageIcon("src/Icon/黑放大.png"));
        decrypt.setBounds(600,30,30,30);
        Tool.add(decrypt);

        //allSPanel.getVerticalScrollBar().setVisible(true);


        //设置滚动条
        allSPanel.setHorizontalScrollBar(null);
        allSPanel.setBorder(null);

        //滚动条配色
        Color c1 = new Color(80,80,80);
        Color c2 = new Color(230,230,230);

        allSPanel.getVerticalScrollBar().setUI(new DemoScrollBarUI(c1,c2,new Dimension(20,0)));
        //缩略图展示
        showPic();

        add(allSPanel);

        //setVisible(true);
        setLocationRelativeTo(null);
    }
    /**添加监听事件 */
    void addListener(){
        int i;
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem moveMenuItem = new JMenuItem("移出保险柜");
        jPopupMenu.add(moveMenuItem);
        for(i = 0;i < PictureNumber; i++){
            ImagesPanel.get(i).addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e){
                    int i = e.getClickCount();
                    int index = ImagesPanel.indexOf(e.getSource());
                    if(i == 2){
                        new safeShowUI(ImagesImage.get(index));
//                        System.out.println(index);
                    }
                    if (e.isMetaDown()){
                        jPopupMenu.show((Component) e.getSource(),e.getX(),e.getY());
//                        System.out.println(index);
                        moveMenuItem.addActionListener(e1 -> movePic(index));
                    }
                }
            });
        }

    }

    public void movePic(int index){

        String name = ImagesFile.get(index).getName();
        int i = name.indexOf(".");
        String houzhui = name.substring(i+1);
//        System.out.println(name.substring(i+1));
        OutputStream ops = null;
        try {
            ops = new FileOutputStream(new File("D:/"+name));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        System.out.println(index);

        try {
            ImageIO.write((BufferedImage) ImagesImage.get(index),houzhui,ops);
        } catch (IOException e) {
            e.printStackTrace();
        }


        ImagesFile.get(index).delete();

        ImagesFile.remove(index);
        ImagesImage.remove(index);
        PictureNumber--;
        //JOptionPane.showMessageDialog(null, "移出成功，已经放入D盘中", "保险柜移出",
        //        JOptionPane.INFORMATION_MESSAGE);
        newSafeUI();
        setVisible(true);
    }

    /** 查看文件夹是否存在 */
    public static void isFolder(){
        folder = new File("保险柜");
        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
            System.out.println("创建保险柜");
        } else {
            System.out.println("保险柜已存在");
        }
    }

    /** Decodingperation 对于图片进行解码 */
    public void Decodingperation(){
        PictureNumber = 0;
        File[] files = folder.listFiles();// 此路径名的所有文件
        int i;
        for (i = 0; i < Objects.requireNonNull(files).length; i++) {
            String fileName = files[i].getName();
            String format = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            //开始查看文件格式程序能够显示的图片格式包括，.JPG、.JPEG、.GIF、.PNG、和.BMP。
            if (format.equals("jpg") || format.equals("gif") || format.equals("bmp") || format.equals("png")|| format.equals("jpeg")) {
                PictureNumber++;
                ImagesFile.add(files[i]);// 进入图像文件队列
                //解密操作
                try {
                    ImagesImage.add(decryption(files[i]));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /** 缩略图展示 */
    public void showPic(){
//        System.out.println(ImagesFile.size());
        isFolder(); //判断文件夹是否存在
        Decodingperation(); //解码操作
        newSafeUI();//刷新缩略图
    }

    /** 缩略图刷新 */
    public void newSafeUI(){

        int i;
        allPanel.removeAll();
        this.repaint();
        ImagesLabel.clear();
        ImagesPanel.clear();
        //开始进行缩略图展示操作
        for (int m = 0; m < PictureNumber; m++) {
            // 创建预览图片
            ImagesLabel.add(new JLabel());
            ImagesPanel.add(new RoundPanel());
        }
        for (i = 0; i < PictureNumber; i++) {
            //设置白色背景，缩略图配色
            ImagesPanel.get(i).setBackground(Color.WHITE);
            ImagesLabel.get(i).setBackground(Color.white);
        }

        //开始缩略图添加
        for (i = 0;i < PictureNumber;i++){
            //计算缩略图大小
//            System.out.println(i);
            ImagesPanel.get(i).setBounds(i % 4 * 300 + 25, 40 + (i / 4 * 220), 250, 200);//设置边界
            ImageIcon ic1 = null;//从路径中构造ImageIcon类
            ic1 = new ImageIcon(ImagesImage.get(i));
            double h1 = ic1.getIconHeight();
            double w1 = ic1.getIconWidth();//图像长宽
            double w,h;
            if ((double) 250/200 > w1/h1){
                h = 200;
                w = w1 * 200 / h1;
            }else {
                w = 250;
                h = h1 * 250 / w1;
            }
            //System.out.println(w+" " +h);

            //尝试添加缩略图
            Image im = ic1.getImage().getScaledInstance((int)w, (int)h, Image.SCALE_DEFAULT);//改变大小
            ImageIcon ic2 = new ImageIcon(im);//重新得到一个固定图片
            try {
                ImagesLabel.get(i).setIcon(ic2);//定义此组件将显示的图标
            } catch (IndexOutOfBoundsException e){
                System.out.println(i);
            }
            ic2.setImageObserver(ImagesLabel.get(i));//设置图像的图像观察者,ImageObserver是异步的事实是非常重要的
            ImagesPanel.get(i).setLayout(new BorderLayout());
            ImagesPanel.get(i).add(ImagesLabel.get(i));
            allPanel.add(ImagesPanel.get(i));


            if (PictureNumber / 4 >= 4){
                allSPanel.getVerticalScrollBar().setVisible(true);// 将此滚动条的值设置为指定的值
                allPanel.setPreferredSize(new Dimension(1175,900+220*(PictureNumber/4 - 3)+ 75));
                allSPanel.getVerticalScrollBar().setUnitIncrement(20);//设置滚动条速度
            } else {
                allPanel.setPreferredSize(new Dimension(1175,870));
                allSPanel.getVerticalScrollBar().setValue(0);// 将此滚动条的值设置为指定的值
                allSPanel.getVerticalScrollBar().setVisible(false);// 将此滚动条的值设置为指定的值
            }
            allSPanel.setBounds(0,30,1200,870);
        }

        addListener();//添加监听事件
//        System.out.println("缩略图");
        allPanel.repaint();
        //this.pack();
    }

    /** 图片加密操作 */
    public void encryption (File f)throws IOException{
        FileInputStream fis = new FileInputStream(f);

        String f1 = folder.getAbsolutePath() +"/" + f.getName();
        FileOutputStream fos = new FileOutputStream(f1);

        int b;

        while( (b = fis.read()) != -1) {
            fos.write(b ^ 123);
        }

        fis.close();
        fos.close();

        File i = new File(f1);
        ImagesFile.add(i);
//        System.out.println(ImagesFile.size());
        Image image = ImageIO.read(f);
        ImagesImage.add(image);
//        System.out.println("图片："+ImagesImage.size());
        PictureNumber++;
        newSafeUI();
    }

    /** 图片解密 */
    public static Image decryption(File f)throws IOException{
        FileInputStream fis = new FileInputStream(f);

        String fName =  f.getName();
        int index = fName.lastIndexOf(".");
        String houzhui = fName.substring(index);

        String newFName = "src/Data/test" + houzhui;
//        System.out.println(newFName);
        File newFile = new File(newFName);
        FileOutputStream fos = new FileOutputStream(newFile);

        int b;

        while( (b = fis.read()) != -1) {
            fos.write(b ^ 123);
        }

        fis.close();
        fos.close();
        return ImageIO.read(newFile);
    }
    /** 测试代码 */
    public static void main(String[] args) {
        new SafeBoxUI().setVisible(true);
    }
}


