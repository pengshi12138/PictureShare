package test.ImageRGB;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.TextField;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowAdapter;


public class TestColorPalette implements AdjustmentListener, TextListener {
    //rgb值
    private int r = 255;

    private int g = 255;

    private int b = 255;

    //最上方显示颜色的容器
    private Panel display;

    //颜色值文本框
    private TextField rColor = new TextField("255");

    private TextField gColor = new TextField("255");

    private TextField bColor = new TextField("255");

    //滚动条
    private Scrollbar sbr;
    private Scrollbar sbg ;
    private Scrollbar sbb ;

    //初始化
    public void init(){
        Frame f = new Frame("调色板");
        f.setLayout(new BorderLayout(5, 5));
        //最上面的颜色显示部分
        display = new Panel();
        display.setBackground(Color.white);
        f.add(display, "Center");
        //下方设置容器
        Panel set = new Panel(new GridLayout(1, 2, 5, 5));
        f.add(set, "South");
        //左边属性panel
        Panel color = new Panel(new GridLayout(3, 2, 5, 5));
        rColor.addTextListener(this);
        gColor.addTextListener(this);
        bColor.addTextListener(this);
        rColor.setName("rColor");
        gColor.setName("gColor");
        bColor.setName("bColor");
        color.add(new Label("   红色："));
        color.add(rColor);
        color.add(new Label("   绿色："));
        color.add(gColor);
        color.add(new Label("   蓝色："));
        color.add(bColor);
        set.add(color, "West");
        //右边滚动条,构造一个新的滚动条，它具有指定的方向、初始值、可见量、最小值和最大值。
        Panel pc = new Panel(new GridLayout(3, 1, 5, 5));
        sbr = new Scrollbar(Scrollbar.HORIZONTAL, 255, 0, 0, 255);
        sbg = new Scrollbar(Scrollbar.HORIZONTAL, 255, 0, 0, 255);
        sbb = new Scrollbar(Scrollbar.HORIZONTAL, 255, 0, 0, 255);
        //注册监听器
        sbr.addAdjustmentListener(this);
        sbg.addAdjustmentListener(this);
        sbb.addAdjustmentListener(this);
        sbr.setName("sbr");
        sbg.setName("sbg");
        sbb.setName("sbb");
        pc.add(sbr);
        pc.add(sbg);
        pc.add(sbb);
        set.add(pc, "Center");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                System.exit(0);
            };
        });
        f.setVisible(true);
        f.setSize(350, 280);
        f.setLocation(450, 200);
    }

    public static void main(String[] args) {
        TestColorPalette tc = new TestColorPalette();
        tc.init();
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        // TODO Auto-generated method stub
        //获取发生监听事件的组件的名称
        String name = ((Scrollbar) e.getSource()).getName();
        //获取发送监听事件的组件的值
        int value = e.getValue();
        if(name.equals("sbr")){
            r = value;
            rColor.setText("" + r);
        }else if(name.equals("sbg")){
            g = value;
            gColor.setText("" + g);
        }else{
            b = value;
            bColor.setText("" + b);
        }
        changeColor();
    }

    @Override
    public void textValueChanged(TextEvent e) {
        // TODO Auto-generated method stub
        String name = ((TextField) e.getSource()).getName();
        int value;
        if(name.equals("rColor")){
            value = Integer.parseInt(rColor.getText());
            r = value;
            sbr.setValue(value);
        }else if(name.equals("gColor")){
            value = Integer.parseInt(gColor.getText());
            g = value;
            sbg.setValue(value);
        }else{
            value = Integer.parseInt(bColor.getText());
            b = value;
            sbb.setValue(value);
        }
        changeColor();
    }
    public void changeColor(){
        Color c = new Color(r, g, b);
        display.setBackground(c);
    }
}